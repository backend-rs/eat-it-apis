'use strict'

// User Module
module.exports = {
   
    type:{
        type:String,
        default:'dislike',
        enum:['like','dislike']
    },
    status:{
        type:String,
        enum:['yummy','delicious','tasty']
    },
    isfollowed:Boolean,
    foodId:String,
    orderId:String,
    buyerId:String,
    sellerId:String

   
   
}