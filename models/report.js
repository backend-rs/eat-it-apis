'use strict'

// User Module
module.exports = {
   
   reasonId:String,
   reasonName:String,
   reportedTo:String,
   reportedBy:String
}