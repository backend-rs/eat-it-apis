"use strict";

// User Module
module.exports = {
  type: {
    type: String,
    default: "user",
    enum: ["user", "admin"],
  },
  status: {
    type: String,
    default: "inActive",
    enum: ["active", "inActive"],
  },
  loginType: {
    type: String,
    default: "app",
    enum: ["app", "google", "facebook"],
  },
  communicationMode: {
    type: String,
    default: "sms",
    enum: ["phone", "sms", "both", "none"],
  },
  accountDetailId: String,

  accountAdded: {
    type: String,
    default: "false",
    enum: ["true", "false"],
  },
  uId: String,
  tempToken: String,
  firstName: {
    type: String,
    lowercase: true,
    trim: true,
  },
  lastName: {
    type: String,
    lowercase: true,
    trim: true,
  },

  email: {
    type: String,
    lowercase: true,
    trim: true,
  },
  image: {
    url: String,
    thumbnail: String,
    resize_url: String,
    resize_thumbnail: String,
  },
  address: {
    line: String,
    pincode: String,
    city: String,
    country: String,
    landmark: String,
    location: {
      type: {
        type: "String",
        required: true,
        enum: ["Point", "LineString", "Polygon"],
        default: "Point",
      },
      coordinates: {
        type: [Number], // [<longitude>, <latitude>]
        index: "2dsphere", // create the geospatial index
      },
    },
  },
  phone: {
    type: String,
    trim: true,
  },
  password: String,
  token: String,
  expiryTime: String,
  otp: String,
  firebaseToken: String,
  totalDishes: Number,
  followerCount: Number,
  follower: [
    {
      id: String,
    },
  ],
};
