"use strict";

module.exports = {
  paymentId: String,
  for:String,
  orderId:String,
  userId: String,
  foodId: String,
  transactionId:String
};
