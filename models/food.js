'use strict'

// User Module
module.exports = {
    isAdvanceFoodPost: {
        type: Boolean
    },
    advanceFoodDate: {
        type: Date
    },
    youWant: {
        type: String,
        default: 'sale',
        enum: ['sale', 'share']
    },
    status: {
        type: String,
        default: 'active',
        enum: ['active', 'inActive']
    },
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    type: {
        type: String,
        default: 'veg',
        enum: ['veg', 'nonVeg', 'langar']
    },
    foodCooked: {
        type: String,
        default: 'restaurant',
        enum: ['restaurant', 'homemade']
    },
    homeDelivery: {
        type: String,
        default: 'yes',
        enum: ['yes', 'no']
    },
    homeDeliveryPrice: Number,
    quantity: Number,
    description: String,
    // cookingTime: String,
    availabilityTime: Number,
    pickupTime: {
        from: String,
        to: String
    },
    price: Number,
    userId: String,
    address: {
        line: String,
        city: String,
        country: String,
        pincode: String,
        landmark: String,
        location: {
            type: {
                type: "String",
                required: true,
                enum: ['Point', 'LineString', 'Polygon'],
                default: 'Point'
            },
            coordinates: {
                type: [Number], // [<longitude>, <latitude>]
                index: "2dsphere", // create the geospatial index
            },
        },
    },
    cuisine: [
        {
            id: String
        }
    ],
    images: [
        {
            url: String,
            thumbnail: String,
            resize_url: String,
            resize_thumbnail: String
        }
    ]

}