'use strict'

// User Module
module.exports = {
   
    accountDetail: {
        accountNo: String,
        accountHolderName:String,
        bankName: String,
        ifscCode: String
    },
    userId:String
   
    
}