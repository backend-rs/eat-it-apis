'use strict'

// User Module
module.exports = {
   
    status:{
        type:String,
        default:'active',
        enum:['active','inactive']
    },
   
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String
    },
}