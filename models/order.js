"use strict";

// User Module
module.exports = {
  status: {
    type: String,
    default: "pending",
    enum: ["pending", "delivered", "confirmed", "rejected"],
  },
  foodId: String,
  buyerId: String,
  sellerId: String,
  orderQuantity: Number,
  total: Number,
  orderId: String,
  otp: String,
  paymentId: String,
};
