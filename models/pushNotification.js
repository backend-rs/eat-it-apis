'use strict'

module.exports={
    notification:{
      title:String,
      body:String
    },
    data:{
      notificationType:String,
      title:String,
      body:String,
      id:String,
      temp:Date
    },
    chatData: {
      orderId:String,
      username: String,
      userId: String,
      isExpired: String,
    },
    to:String,
    messageId:String,
    foodId:String,
    orderCode:String,
    orderId:String,

}