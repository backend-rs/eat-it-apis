'use strict'

exports.toModel = entity => {
   var model={
       _id:entity._id,
       status:entity.status,
       buyerId:entity.buyerId,
       sellerId:entity.sellerId,
       foodId:entity.foodId,
       otp:entity.otp,
       orderId:entity.orderId
   }
//    if(entity.image){
//        model.image={
//            url:entity.image.url,
//            resize_url:entity.image.resize_url,
//            thumbnail:entity.image.thumbnail,
//            resize_thumbnail:entity.image.resize_thumbnail
//        }
//    }
   return model
}