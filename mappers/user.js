'use strict'

// users create mapper
exports.toModel = entity => {

    var model = {
        _id: entity._id,
        type: entity.type,
        firstName: entity.firstName,
        lastName: entity.lastName,
        loginType:entity.loginType,
        token:entity.token,
        uId:entity.uId,
        tempToken:entity.tempToken,
        email: entity.email,
        phone: entity.phone,
        totalDishes:entity.totalDishes,
        followerCount:entity.followerCount,
        communicationMode:entity.communicationMode,
        address:entity.address

    }
   
    if (entity.image) {
        model.image = {
            url: entity.image.url,
            thumbnail: entity.image.thumbnail,
            resize_url: entity.image.resize_url,
            resize_thumbnail: entity.image.resize_thumbnail
        }
    }
    return model
}

// for particular user
exports.toGetUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        firstName: entity.firstName,
        lastName: entity.lastName,
        firmName: entity.firmName,
        email: entity.email,
        phone: entity.phone,
        address:entity.address,
        totalDishes:entity.totalDishes,
        followerCount:entity.followerCount,
        communicationMode:entity.communicationMode
    }
   
    if (entity.image) {
        model.image = {
            url: entity.image.url,
            thumbnail: entity.image.thumbnail,
            resize_url: entity.image.resize_url,
            resize_thumbnail: entity.image.resize_thumbnail
        }
    }
    return model
}

// for login 
exports.toUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        status:entity.status,
        isVerified: entity.isVerified,
        userName: entity.userName,
        firstName: entity.firstName,
        lastName: entity.lastName,
        firmName: entity.firmName,
        email: entity.email,
        address:entity.address,
        token: entity.token,
        phone: entity.phone,
        totalDishes:entity.totalDishes,
        followerCount:entity.followerCount,
        communicationMode:entity.communicationMode
    }
    return model
}