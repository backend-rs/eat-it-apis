'use strict'

exports.toModel = entity => {
   var model={
       _id:entity._id,
       name:entity.name,
       youWant:entity.youWant,
       type:entity.type,
       homeDelivery:entity.homeDelivery,
       homeDeliveryPrice:entity.homeDeliveryPrice,
       quantity:entity.quantity,
       description:entity.description,
       cookingTime:entity.cookingTime,
       availabilityTime:entity. availabilityTime,
       cookingDate:entity.cookingDate,
       pickupTime:entity.pickupTime,
       price:entity.price,
       userId:entity.userId,
       images:entity.images,
       foodCooked:entity.foodCooked,
       cuisine:entity.cuisine,
       isAdvanceFoodPost: entity.isAdvanceFoodPost || false,
       advanceFoodDate: entity.advanceFoodDate
   }
  if (entity.address) {
        model.address = entity.address
      }
//  images:entity.images
   return model
}

// exports.toGetUser = (entities) => {
//    return entities.map(entity => {
//        return exports.toModel(entity)
//    })
// }