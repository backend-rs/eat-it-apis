var socketio = require("socket.io");
var mongoose = require("mongoose");
var events = require("events");
var _ = require("lodash");
var eventEmitter = new events.EventEmitter();
console.log("connected");
require("../models/rooms.js");
var moment = require("moment");
const service = require("../services/pushNotification");

var roomModel = mongoose.model("Room");

require("../models/chats.js");
var chatModel = mongoose.model("Chat");

module.exports.sockets = function (http) {
  io = socketio.listen(http);

  var ioChat = io;
  let userStack = [];
  let usersStack = [];
  let oldChats = [];
  var sendOldChats, sendUserStack, setRoom;
  var userSocket = {};
  var socketUsers = [];
  var userId;

  //socket.io magic starts here
  ioChat.on("connection", function (socket) {
    console.log("socketio chat connected.");

    //function to get user name
    socket.on("set-user-data", function (username, id) {
      console.log(username + "  logged In");
      //storing variable.
      socketUsers.push({
        username: username,
        id: id,
      });

      socket.username = username;
      socket.userId = id;
      userId = id;

      userSocket[socket.username] = socket.id;
      console.log("socketId", userSocket[socket.username]);
      //getting all users list
      // eventEmitter.emit("get-all-users", id);

      //sending all users list. and setting if online or offline.
      sendUserStack = function () {
        ioChat.to(userSocket[socket.username]).emit("onlineStack", userStack);
      };
    }); //end of set-user-data event.

    //setting room.
    socket.on("set-room", function (room) {
      //leaving room.
      socket.leave(socket.room);
      //getting room data.
      eventEmitter.emit("get-room-data", room);
      //setting room and join.
      setRoom = function (roomId) {
        socket.room = roomId;
        console.log("roomId : " + socket.room);
        socket.join(socket.room);
        ioChat.to(userSocket[socket.username]).emit("set-room", socket.room);
      };
    }); //end of set-room event.

    // old chat.
    socket.on("old-chats", function (room) {
      eventEmitter.emit("get-old-chats", room);
      console.log("oldChatss up");
      sendOldChats = function () {
        ioChat
          .to(userSocket[socket.username])
          .emit("old-chats", { room: room.room, result: oldChats });
      };
    }); // end of old chat

    //showing msg on typing.
    socket.on("typing", function () {
      socket.to(socket.room).broadcast.emit("typing", " typing...");
    });

    //for showing chats.
    socket.on("chat-msg", function (data) {
      //emits event to save chat to database.
      eventEmitter.emit("save-chat", {
        msgFromId: data.msgFromId,
        msgFrom: socket.username,
        msgToId: data.msgToId,
        orderId: data.orderId,
        msgTo: data.msgTo,
        msg: data.msg,
        room: socket.room,
        date: data.date,
      });

      let msgTime = moment(data.date).format("LT");
      let msgDate = moment(data.date).format("L");
      //emits event to send chat msg to all clients.
      console.log("msgDate", msgDate);
      ioChat.to(socket.room).emit("chat-msg", {
        msgFromId: data.msgFromId,
        msgToId: data.msgToId,
        msgFrom: socket.username,
        orderId: data.orderId,
        msg: data.msg,
        time: msgTime,
        date: msgDate,
      });
    });

    //for popping disconnection message.
    socket.on("disconnect", async function (room) {
      // set current user to offline
      socket.leave(room);
      console.log("User left " + room);

      // console.log(socket.username + "  logged out");
      // socket.broadcast.emit("broadcast", {
      //   description: socket.username + " Logged out",
      // });
      // console.log("chat disconnected.");
      // _.unset(userSocket, socket.username);
      // for (const user of userStack) {
      //   if (user.userId === socket.userId) {
      //     user.status = "Offline";
      //   }
      // }
      // ioChat.to(userSocket[socket.username]).emit("onlineStack", userStack);
    }); //end of disconnect event.
  }); //end of io.on(connection).

  //saving chats to database.
  eventEmitter.on("save-chat", function (data) {
    console.log("save-chat:", data);
    // var today = Date.now();
    let isExpired = false;
    let tempOrder = {};
    db.order
      .findOne({
        orderId: {
          $eq: data.orderId,
        },
      })
      .exec(function (err, order) {
        if (err) {
          console.log("stack user error", err);
        } else {
          tempOrder = order;
          console.log(tempOrder);
        }
      });
    try {
      var newChat = new chatModel({
        msgFromId: data.msgFromId,
        msgFrom: data.msgFrom,
        msgToId: data.msgToId,
        msgTo: data.msgTo,
        orderId: data.orderId,
        msg: data.msg,
        room: data.room,
        createdOn: data.date,
      });
    } catch (err) {
      console.log(err);
    }
    newChat.save(function (err, result) {
      if (err) {
        console.log("Error : " + err);
      } else if (result == undefined || result == null || result == "") {
        console.log("Chat Is Not Saved.");
      } else {
        let tempname = result.msgFrom
          .toLowerCase()
          .split(" ")
          .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
          .join(" ");
        if (tempOrder.availabilityTime == 0) {
          isExpired = true;
        } else {
          isExpired = false;
        }
        let temp = {
          notification: {
            title: ` ${tempname} (#${result.orderId})`,
            body: `${result.msg} `,
          },
          data: {
            notificationType: "Chat",
            title: ` ${tempname} (#${result.orderId})`,
            body: `${result.msg} `,
          },
          from: "chatData",
          chatData: {
            orderId: `${result.orderId}`,
            userName: result.msgFrom,
            userId: result.msgToId,
            isExpired: isExpired,
          },
          orderCode: `${result.orderId}`,
          to: `${result.msgToId}`,
          orderId: `${result._id}`,
        };

        service.create("", temp, "", "");
        console.log("Chat Saved", result);
      }
    });
  });

  eventEmitter.on("get-all-users", function (id) {
    console.log("get-all-users");
    let isSms = false;
    let isPhone = false;
    let isExpired = false;
    let tempMsg;
    let temptime;
    let tempstackUser = {};

    db.user.findById(id).exec(function (err, user) {
      if (err) {
        console.log("stack user error", err);
      } else {
        tempstackUser = user;
        console.log(user.firstName);
      }
    });
    db.order
      .find({
        $or: [{ buyerId: id }, { sellerId: id }],
        status: {
          $in: ["delivered", "confirmed"],
        },
      })
      .exec(function (err, orders) {
        if (err) {
          console.log("Error : " + err);
        } else {
          if (userStack.length != 0) {
            userStack = [];
          }
          if (orders.length > 0) {
            orders.forEach((order) => {
              console.log("orderasssss:", order);
              let userData = {};
              let userDetial = {};
              if (order.buyerId == id) {
                db.user
                  .findById(order.sellerId)
                  .exec(function (err, userDetails) {
                    if (err) {
                      console.log("errrrrrr", error);
                    } else {
                      userData = userDetails;
                      console.log("user data::", userDetails);
                      db.chats
                        .findOne({
                          // $or: [
                          //   { msgFromId: userStack._id}, {msgToId: userDetails._id },
                          //   { msgFromId: userDetails._id}, {msgToId: userStack._id },
                          // ],
                          orderId: {
                            $eq: order.orderId,
                          },
                        })
                        .sort({ _id: -1 })
                        .limit(1)
                        .exec(function (err, msges) {
                          if (err) {
                            console.log("msg errorrr::", err);
                          } else {
                            if (
                              tempstackUser.communicationMode == "phone" &&
                              userData.communicationMode == "phone"
                            ) {
                              isPhone = true;
                            }
                            if (
                              tempstackUser.communicationMode == "sms" &&
                              userData.communicationMode == "sms"
                            ) {
                              isSms = true;
                            } 
                             if (
                              tempstackUser.communicationMode == "both" &&
                              userData.communicationMode == "both"
                            ) {
                              isSms = true;
                              isPhone = true;
                            }
                            if (
                              tempstackUser.communicationMode == "none" &&
                              userData.communicationMode == "none"
                            ) {
                              isSms = false;
                              isPhone = false;
                            }
                            if (msges == null || msges == undefined) {
                              (tempMsg = ""), (temptime = "");
                            } else {
                              (tempMsg = msges.msg),
                                (temptime = msges.createdOn);
                            }
                            if (order.availabilityTime == 0) {
                              isExpired = true;
                            } else {
                              isExpired = false;
                            }
                            userDetial = {
                              msg: tempMsg,
                              time: temptime,
                              isPhone: isPhone,
                              isSms: isSms,
                              username: userData.firstName,
                              lastname: userData.lastName,
                              status: order.status,
                              communicationMode: userData.communicationMode,
                              phone: userData.phone,
                              userId: userData._id,
                              orderId: order.orderId,
                              isExpired: isExpired,
                              timeStamp: order.timeStamp,
                            };
                            userStack.push(userDetial);
                            console.log("buyerrr userrr::", userStack);
                          }
                        });
                    }
                  });
              } else if (order.sellerId == id) {
                db.user
                  .findById(order.buyerId)
                  .exec(function (err, userDetails) {
                    if (err) {
                      console.log("errrrrrr", error);
                    } else {
                      userData = userDetails;
                      console.log("user data::", userDetails);
                      db.chats
                        .findOne({
                          // $or: [
                          //   { msgFromId: userStack._id}, {msgToId: userDetails._id },
                          //   { msgFromId: userDetails._id}, {msgToId: userStack._id },
                          // ],
                          orderId: {
                            $eq: order.orderId,
                          },
                        })
                        .sort({ _id: -1 })
                        .limit(1)
                        .exec(function (err, msges) {
                          if (err) {
                            console.log("msg errorrr::", err);
                          } else {
                            if (
                              tempstackUser.communicationMode == "phone" &&
                              userData.communicationMode == "phone"
                            ) {
                              isPhone = true;
                            }
                            if (
                              tempstackUser.communicationMode == "sms" &&
                              userData.communicationMode == "sms"
                            ) {
                              isSms = true;
                            }
                            if (
                              tempstackUser.communicationMode == "both" &&
                              userData.communicationMode == "both"
                            ) {
                              isSms = true;
                              isPhone = true;
                            }
                            if (
                              tempstackUser.communicationMode == "none" &&
                              userData.communicationMode == "none"
                            ) {
                              isSms = false;
                              isPhone = false;
                            }
                            if (msges == null || msges == undefined) {
                              (tempMsg = ""), (temptime = "");
                            } else {
                              (tempMsg = msges.msg),
                                (temptime = msges.createdOn);
                            }
                            if (order.availabilityTime == 0) {
                              isExpired = true;
                            } else {
                              isExpired = false;
                            }
                            userDetial = {
                              msg: tempMsg,
                              time: temptime,
                              isPhone: isPhone,
                              isSms: isSms,
                              username: userData.firstName,
                              lastname: userData.lastName,
                              communicationMode: userData.communicationMode,
                              status: order.status,
                              phone: userData.phone,
                              userId: userData._id,
                              isExpired: isExpired,
                              orderId: order.orderId,
                              timeStamp: order.timeStamp,
                            };
                            userStack.push(userDetial);
                            let tempSort = userStack.sort(function (a, b) {
                              return (
                                new Date(b.timeStamp) - new Date(a.timeStamp)
                              );
                            });

                            userStack = tempSort;
                            console.log("sellerrr::", userStack);
                            console.log("stack " + Object.keys(userStack));
                            console.log(userStack);
                            sendUserStack();
                          }
                        });
                    }
                  });
              }
            });
          }
        }
      });
  });

  eventEmitter.on("get-old-chats", function (data) {
    console.log("get-old-chatssssssssssssssssssss");
    db.chats
      .find({
        room: {
          $eq: data.room,
        },
      })
      .exec(function (err, chats) {
        if (err) {
          console.log("Error : " + err);
        } else {
          if (chats.length != 0) {
            let tempchats = chats.sort(function (a, b) {
              return new Date(b.createdOn) - new Date(a.createdOn);
            });
            if (oldChats.length != 0) {
              oldChats = [];
            }

            tempchats.forEach((chat) => {
              let chatDetial = {
                chatId: chat._id,
                msgFrom: chat.msgFrom,
                msgTo: chat.msgTo,
                msg: chat.msg,
                room: chat.room,
                date: chat.createdOn,
              };
              oldChats.push(chatDetial);
            });
            console.log("stack " + Object.keys(oldChats));
            sendOldChats();
          } else {
            oldChats = [];
            sendOldChats();
          }
        }
      });
  });

  //listening get-room-data event.
  eventEmitter.on("get-room-data", function (room) {
    console.log("get-room-data:", room);
    db.rooms.find(
      {
        $or: [
          {
            name1: room.name1,
          },
          {
            name1: room.name2,
          },
          {
            name2: room.name1,
          },
          {
            name2: room.name2,
          },
        ],
      },
      function (err, result) {
        if (err) {
          console.log("Error : " + err);
        } else {
          if (result == "" || result == undefined || result == null) {
            var today = Date.now();

            newRoom = new roomModel({
              name1: room.name1,
              name2: room.name2,
              lastActive: today,
              createdOn: today,
            });

            newRoom.save(function (err, newResult) {
              if (err) {
                console.log("Error : " + err);
              } else if (
                newResult == "" ||
                newResult == undefined ||
                newResult == null
              ) {
                console.log("Some Error Occured During Room Creation.");
              } else {
                setRoom(newResult._id); //calling setRoom function.
              }
            }); //end of saving room.
          } else {
            var jresult = JSON.parse(JSON.stringify(result));
            setRoom(jresult[0]._id); //calling setRoom function.
          }
        } //end of else.
      }
    ); //end of find room.
  }); //end of get-room-data listener.
};
