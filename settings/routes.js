"use strict";

const fs = require("fs");
const specs = require("../specs");
const api = require("../api");
var auth = require("../permit");
const validator = require("../validators");
const fileUpload = require("express-fileupload");
const bodyParser = require("body-parser");

var multipart = require("connect-multiparty");
var multipartMiddleware = multipart();

const configure = (app, logger) => {
  const log = logger.start("settings:routes:configure");

  app.get("/specs", function (req, res) {
    fs.readFile("./public/specs.html", function (err, data) {
      if (err) {
        return res.json({
          isSuccess: false,
          error: err.toString(),
        });
      }
      res.contentType("text/html");
      res.send(data);
    });
  });

  app.get("/api/specs", function (req, res) {
    res.contentType("application/json");
    res.send(specs.get());
  });

  // .......................users routes..............................
  app.post("/api/users/addNewField", api.users.addField);
  app.get(
    "/api/users",
    auth.context.builder,
    auth.context.requiresToken,
    api.users.get
  );
  app.get(
    "/api/users/socketUsers",
    auth.context.builder,
    api.users.getSocketUsers
  );

  app.post(
    "/api/users",
    auth.context.builder,
    validator.users.canCreate,
    api.users.create
  );
  app.get(
    "/api/users/:id",
    auth.context.builder,
    auth.context.requiresToken,
    validator.users.getById,
    api.users.getById
  );
  app.put(
    "/api/users/:id",
    auth.context.builder,
    auth.context.requiresToken,
    validator.users.update,
    api.users.update
  );

  app.post(
    "/api/users/login",
    auth.context.builder,
    validator.users.login,
    api.users.login
  );

  app.post(
    "/api/users/forgotPassword",
    auth.context.builder,
    validator.users.forgotPassword,
    api.users.forgotPassword
  );
  app.post(
    "/api/users/resetPassword",
    auth.context.builder,
    validator.users.resetPassword,
    api.users.resetPassword
  );
  app.post(
    "/api/users/changePassword",
    auth.context.builder,
    auth.context.requiresToken,
    validator.users.changePassword,
    api.users.changePassword
  );
  app.post(
    "/api/users/logOut",
    auth.context.builder,
    auth.context.requiresToken,
    api.users.logOut
  );

  // .......................orders routes..............................
  app.get(
    "/api/orders",
    auth.context.builder,
    auth.context.requiresToken,
    api.orders.get
  );

  app.post(
    "/api/orders",
    auth.context.builder,
    auth.context.requiresToken,
    validator.orders.canCreate,
    api.orders.create
  );
  app.get(
    "/api/orders/:id",
    auth.context.builder,
    auth.context.requiresToken,
    validator.orders.getById,
    api.orders.getById
  );
  app.put(
    "/api/orders/:id",
    auth.context.builder,
    auth.context.requiresToken,
    validator.orders.update,
    api.orders.update
  );

  // ................................upload files............................................
  app.post(
    "/api/files",
    auth.context.builder,
    multipartMiddleware,
    api.files.create
  );
  app.post("/api/files/upload", multipartMiddleware, api.files.upload);
  app.get("/api/files/:id", auth.context.builder, api.files.getById);

  // .................................cuisine.....................................
  app.post(
    "/api/cuisines",
    auth.context.builder,
    auth.context.requiresToken,
    validator.cuisines.canCreate,
    api.cuisines.create
  );
  app.put(
    "/api/cuisines/:id",
    auth.context.builder,
    auth.context.requiresToken,
    api.cuisines.update
  );
  app.get("/api/cuisines", auth.context.builder, api.cuisines.get);
  // ........................chat..............................................
  app.get("/api/chats", auth.context.builder, api.chat.get);

  // .................................rating.....................................
  app.post(
    "/api/ratings",
    auth.context.builder,
    auth.context.requiresToken,
    api.ratings.create
  );
  app.get("/api/ratings", auth.context.builder, api.ratings.get);

  // ................................report.....................................
  app.post(
    "/api/reports",
    auth.context.builder,
    auth.context.requiresToken,
    api.reports.create
  );
  app.get("/api/reports", auth.context.builder, api.reports.get);

  // .................................reason.....................................
  app.post(
    "/api/reasons",
    auth.context.builder,
    auth.context.requiresToken,
    api.reasons.create
  );
  app.get("/api/reasons", auth.context.builder, api.reasons.get);

  // .................................userAccount.....................................
  app.post(
    "/api/userAccounts",
    auth.context.builder,
    auth.context.requiresToken,
    api.userAccounts.create
  );
  app.get("/api/userAccounts", auth.context.builder,auth.context.requiresToken, api.userAccounts.get);

  // .................................favorite.....................................
  app.post(
    "/api/favorites",
    auth.context.builder,
    auth.context.requiresToken,
    api.favorites.create
  );
  app.get("/api/favorites", auth.context.builder, api.favorites.get);
  app.put(
    "/api/favorites/:id",
    auth.context.builder,
    auth.context.requiresToken,
    api.favorites.update
  );

  // .................................foods.....................................
  app.post(
    "/api/foods",
    auth.context.builder,
    auth.context.requiresToken,
    validator.foods.canCreate,
    api.foods.create
  );
  app.get("/api/foods/search", auth.context.builder, api.foods.search);
  app.put(
    "/api/foods/update/:id",
    auth.context.builder,
    auth.context.requiresToken,
    api.foods.update
  );
  app.get(
    "/api/foods/:id",
    auth.context.builder,
    validator.foods.getById,
    api.foods.getById
  );
  app.get("/api/foods", auth.context.builder, api.foods.get);

  // .............................sendNotifications...............................
  app.get(
    "/api/pushNotification",
    auth.context.builder,
    auth.context.requiresToken,
    api.pushNotification.get
  );
  app.post(
    "/api/pushNotification",
    auth.context.builder,
    auth.context.requiresToken,
    api.pushNotification.create
  );

  // ................................payments routes......................
  app.post("/api/payments/token", auth.context.builder, api.payments.token);
  app.post(
    "/api/payments/payment",
    auth.context.builder,
    api.payments.paypalPayment
  );
  app.get("/api/payments/success", auth.context.builder, api.payments.success);
  app.get("/api/payments/cancel", auth.context.builder, api.payments.cancel);
  app.post(
    "/api/payments/paytmInitiateTokenOrStatus",
    auth.context.builder,
    api.payments.paytmInitiateTokenOrStatus
  );
  app.post(
    "/api/payments/createPayment",
    auth.context.builder,
    api.payments.createPayment
  );

  // app.get("/api/foods", auth.context.builder, api.foods.get);
  // app.post("/api/payments/execute", auth.context.builder, api.payments.execute);
  // app.get("/api/payments", auth.context.requiresToken, api.payments.get);
  // app.get(
  //   "/api/payments/:id",
  //   auth.context.builder,
  //   auth.context.requiresToken,
  //   validator.payments.getById,
  //   api.payments.getById
  // );

  log.end();
};
exports.configure = configure;
