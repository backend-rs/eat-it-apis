'use strict'
const path = require('path')
const cors = require('cors')
const express = require('express')

const bodyParser = require('body-parser')
const logger = require('@open-age/logger')('server')

const configure = (app) => {
    const log = logger.start('settings/express/configure')

    app.use(bodyParser.json())
    app.use(cors())

    app.use(bodyParser.urlencoded({
        extended: true
    }))

    app.use(bodyParser({
        limit: '50mb',
        keepExtensions: true
    }))

    const root = path.normalize(__dirname + './../')
    app.use(express.static(path.join(root, 'public')))

    log.end()
}

exports.configure = configure














// exports.configure = configure
//   const log = logger.start("settings:routes:configure");

//   app.get("/specs", function (req, res) {
//     fs.readFile("./public/specs.html", function (err, data) {
//       if (err) {
//         return res.json({
//           isSuccess: false,
//           error: err.toString(),
//         });
//       }
//       res.contentType("text/html");
//       res.send(data);
//     });
//   });

//   app.get("/api/specs", function (req, res) {
//     res.contentType("application/json");
//     res.send(specs.get());
//   });

//   // .......................users routes..............................
//   app.post(
//     "/api/users",
//     auth.context.builder,
//     validator.users.canCreate,
//     api.users.create
//   );
//   app.get(
//     "/api/users/:id",
//     auth.context.builder,
//     auth.context.requiresToken,
//     validator.users.getById,
//     api.users.getById
//   );
//   app.put(
//     "/api/users/:id",
//     auth.context.builder,
//     auth.context.requiresToken,
//     validator.users.update,
//     api.users.update
//   );

//   app.post(
//     "/api/users/login",
//     auth.context.builder,
//     validator.users.login,
//     api.users.login
//   );

//   app.post(
//     "/api/users/forgotPassword",
//     auth.context.builder,
//     validator.users.forgotPassword,
//     api.users.forgotPassword
//   );
//   app.post(
//     "/api/users/resetPassword",
//     auth.context.builder,
//     validator.users.resetPassword,
//     api.users.resetPassword
//   );
//   app.post(
//     "/api/users/changePassword",
//     auth.context.builder,
//     auth.context.requiresToken,
//     validator.users.changePassword,
//     api.users.changePassword
//   );
//   app.post(
//     "/api/users/logOut",
//     auth.context.builder,
//     auth.context.requiresToken,
//     api.users.logOut
//   );

//   // .......................orders routes..............................
//   app.get(
//     "/api/orders",
//     auth.context.builder,
//     auth.context.requiresToken,
//     api.orders.get
//   );

//   app.post(
//     "/api/orders",
//     auth.context.builder,
//     auth.context.requiresToken,
//     validator.orders.canCreate,
//     api.orders.create
//   );
//   app.get(
//     "/api/orders/:id",
//     auth.context.builder,
//     auth.context.requiresToken,
//     validator.orders.getById,
//     api.orders.getById
//   );
//   app.put(
//     "/api/orders/:id",
//     auth.context.builder,
//     auth.context.requiresToken,
//     validator.orders.update,
//     api.orders.update
//   );

//   // ................................upload files............................................
//   app.post(
//     "/api/files",
//     auth.context.builder,
//     multipartMiddleware,
//     api.files.create
//   );
//   app.post("/api/files/upload", multipartMiddleware, api.files.upload);
//   app.get("/api/files/:id", auth.context.builder, api.files.getById);

//   // .................................cuisine.....................................
//   app.post(
//     "/api/cuisines",
//     auth.context.builder,
//     auth.context.requiresToken,
//     validator.cuisines.canCreate,
//     api.cuisines.create
//   );
//   app.get("/api/cuisines", auth.context.builder, api.cuisines.get);

//   // .................................rating.....................................
//   app.post(
//     "/api/ratings",
//     auth.context.builder,
//     auth.context.requiresToken,
//     api.ratings.create
//   );
//   app.get("/api/ratings", auth.context.builder, api.ratings.get);

//   // .................................favorite.....................................
//   app.post(
//     "/api/favorites",
//     auth.context.builder,
//     auth.context.requiresToken,
//     api.favorites.create
//   );
//   app.get("/api/favorites", auth.context.builder, api.favorites.get);

//   // .................................foods.....................................
//   app.post(
//     "/api/foods",
//     auth.context.builder,
//     auth.context.requiresToken,
//     validator.foods.canCreate,
//     api.foods.create
//   );
//   app.get("/api/foods/search", auth.context.builder, api.foods.search);

//   app.get(
//     "/api/foods/:id",
//     auth.context.builder,
//     validator.foods.getById,
//     api.foods.getById
//   );
//   app.get("/api/foods", auth.context.builder, api.foods.get);

//   // ................................payments routes......................
//   app.post("/api/payments/checkout", auth.context.builder, api.payments.create);
//   app.post("/api/payments/pay", api.payments.payment);
//   app.get(
//     "/api/payments/token",
//     auth.context.builder,
//     auth.context.requiresToken,
//     api.payments.paymentToken
//   );
//   app.get("/api/payments", auth.context.requiresToken, api.payments.get);
//   app.get(
//     "/api/payments/:id",
//     auth.context.builder,
//     auth.context.requiresToken,
//     validator.payments.getById,
//     api.payments.getById
//   );

//   app.get("/paypal", auth.context.builder, api.paypal.getAll);
//   app.get("/success", auth.context.builder, api.paypal.success);
//   app.get("/cancel", auth.context.builder, api.paypal.cancel);

//   log.end();
// };