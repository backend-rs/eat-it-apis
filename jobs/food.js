"use strict";
const cron = require("cron").CronJob;
const moment = require("moment");
const service = require("../services/foods");
const start = async (startOn) => {
  let job = new cron({
    cronTime: startOn,
    onTick: async () => {
      let now = moment(new Date());
      console.log("running a task daily.");
      let foods = await db.food.find();

      if (foods.length != 0) {
        for (const food of foods) {
          if (food) {
            console.log(moment(food.advanceFoodDate).format('DD-MM-YYYY'))
            console.log(moment(new Date()).format('DD-MM-YYYY'))
            if(food.isAdvanceFoodPost == false) {
              if (food.availabilityTime > 0 ) {
                let foodavailable = food.availabilityTime;
                food.availabilityTime = foodavailable - 1;
              }
              if (food.availabilityTime == 0) {
                food.status = "inActive";
              }
            } else if ((moment(food.advanceFoodDate).format('DD-MM-YYYY') == moment(new Date()).format('DD-MM-YYYY')) && food.isAdvanceFoodPost == true) {
              food.isAdvanceFoodPost = false
            }
            // expireDate = moment(now).add(food.availabilityTime, "days");
            // var dd = expireDate.format("DD-MM-YYYY");
            // now = moment(new Date()).format("DD-MM-YYYY");

            // if (dd == now && food.availabilityTime == 0) {
            //   food.status = "inActive";
            // }
            food.save();
          }
        }
      }
      console.log("foods update");
    },
    start: true,
  });
};
console.log("After job instantiation");
exports.schedule = () => {
  // start(`1 * * * * *`);
  start(`0 5 * * *`);
};
