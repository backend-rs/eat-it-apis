
const response = require('../exchange/response')
const service = require('../services/foods')
const mapper = require('../mappers/food')
const api = require('./api-base')('foods', 'food')
const message = require('../helpers/message');
const pagination = require('../helpers/paging');



api.search= async (req, res) => {
    const log = req.context.logger.start('api/foods/search')
    try {
        if (!service.search) {
            throw new Error(message.methodError)
        }
        const page = pagination.pager(req);
        const food= await service.search(req,page,req.context)
        log.end()
        return response.authorized(res, food)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}




module.exports =api