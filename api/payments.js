const response = require("../exchange/response");
const service = require("../services/payments");
const api = require("./api-base")("payments", "payment");

api.token = async (req, res) => {
  try {
    const token = await service.token(req, res);
    return response.data(res, token);
  } catch (err) {
    return response.failure(res, err.message);
  }
};

api.paypalPayment = async (req, res) => {
  try {
    const payment = await service.paypalPayment(req, req.body, res);
    return response.data(res, payment);
  } catch (err) {
    return response.failure(res, err.message);
  }
};

api.success = async (req, res) => {
  try {
    const success = await service.success(req, res);
  } catch (err) {
    return response.failure(res, err.message);
  }
};

api.cancel = async (req, res) => {
  try {
    const success = await service.cancel(req, res);
  } catch (err) {
    return response.failure(res, err.message);
  }
};

api.paytmInitiateTokenOrStatus = async (req, res) => {
  try {
    const token = await service.paytmInitiateTokenOrStatus(req.body, res);
    return response.data(res, token);
  } catch (err) {
    return response.failure(res, err.message);
  }
};
api.createPayment = async (req, res) => {
  try {
    const createPayment = await service.createPayment(req.body, res);
    return response.data(res, createPayment);
  } catch (err) {
    return response.failure(res, err.message);
  }
};

module.exports = api;

// api.execute = async (req, res) => {
//     try {
//       const execute = await service.execute(req, req.body, res);
//       return response.data(res, execute);
//     } catch (err) {
//       return response.failure(res, err.message);
//     }
//   };

// api.getAll = async (req, res) => {
//     try {
//       const user = await service.getAll(req, res);
//     } catch (err) {
//       return response.failure(res, err.message);
//     }
//   };

// const response = require('../exchange/response')
// const service = require('../services/payments')
// const mapper = require('../mappers/payment')
// const api = require('./api-base')('payments', 'payment');
// const message = require('../helpers/message');

// api.payment = async (req, res) => {
//     // const log = req.context.logger.start('api/payments/capture')
//     try {
//         if (!service.payment) {
//             throw new Error(message.methodError)
//         }
//         const payment = await service.payment(req.body, req.context)
//         log.end()
//         return response.authorized(res, payment)
//     } catch (err) {
//         log.error(err.message)
//         log.end()
//         return response.failure(res, err.message)
//     }
// }

// api.paymentToken = async (req, res) => {
//     const log = req.context.logger.start('api/payments/paymentToken')
//     try {
//         if (!service.paymentToken ) {
//             throw new Error(message.methodError)
//         }
//         const payment = await service.paymentToken(req.body, req.context)
//         log.end()
//         return response.authorized(res, payment)
//     } catch (err) {
//         log.error(err.message)
//         log.end()
//         return response.failure(res, err.message)
//     }
// }
// module.exports = api
