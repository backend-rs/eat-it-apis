"use strict";

const response = require("../exchange/response");
const service = require("../services/users");
const mapper = require("../mappers/user");
const api = require("./api-base")("users", "user");
const message = require("../helpers/message");

api.login = async (req, res) => {
  const log = req.context.logger.start("api/users/login");
  try {
    if (!service.login) {
      throw new Error(message.methodError);
    }
    const user = await service.login(req.body, req.context);
    log.end();
    return response.authorized(res, mapper.toUser(user));
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};

api.changePassword = async (req, res) => {
  const log = req.context.logger.start(`api/users/changePassword`);
  try {
    if (!service.changePassword) {
      throw new Error(message.methodError);
    }
    const user = await service.changePassword(req.body, req.context);
    log.end();
    return response.data(res, message.changeMessage);
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};

api.forgotPassword = async (req, res) => {
  const log = req.context.logger.start(`api/users/forgotPassword`);
  try {
    if (!service.forgotPassword) {
      throw new Error(message.methodError);
    }
    const user = await service.forgotPassword(req.body, req.context);
    log.end();
    return response.data(res, message.otpMessage);
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};

api.resetPassword = async (req, res) => {
  const log = req.context.logger.start(`api/users/verifyOtp`);
  try {
    if (!service.resetPassword) {
      throw new Error(message.methodError);
    }
    const user = await service.resetPassword(req.body, req.context);
    log.end();
    return response.data(res, message.resetMessage);
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};

api.logOut = async (req, res) => {
  const log = req.context.logger.start("api/users/logout");
  try {
    if (!service.logOut) {
      throw new Error(message.methodError);
    }
    const user = await service.logOut(res, req.context);
    log.end();
    return response.data(res, mapper.toModel(user));
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};
api.addField = async (req, res) => {
  try {
    const user = await service.addField();
    return response.data(res, user);
  } catch (err) {
    return response.failure(res, err.message);
  }
};
api.getSocketUsers = async (req, res) => {
  const log = req.context.logger.start("api/users/getSocketUsers");
  try {
    if (!service.getSocketUsers) {
      throw new Error(message.methodError);
    }
    const user = await service.getSocketUsers(req, req.context);
    log.end();
    return response.data(res, user);
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};
module.exports = api;
