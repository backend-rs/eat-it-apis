exports.methodError = "METHOD_NOT_SUPPORTED";
exports.otpMessage = "OTP_SEND_SUCCESSFULLY";
exports.changeMessage = "PASSWORD_CHANGED_SUCCESSFULLY";
exports.resetMessage = "PASSWORD_RESET_SUCCESSFULLY";
exports.userError = "USER_NOT_FOUND";
exports.userExists = "USER_ALREADY_EXISTS";
exports.logOutMessage = "LOGOUT_SUCCESSFULLY";
exports.passwordError = "PASSWORD_MISMATCH";
exports.compareOldPassword = "OLD_PASSWORD_DID_NOT_MATCH";
exports.emailValidationError = "INVAILD_EMAIL";
exports.phoneValidationError = "INVALID_PHONE";
exports.userRegistrationMessage =
  "YOUR_REQUEST_HAS_BEEN_SUBMITTED_SUCCESSFULLY";
exports.alreadySubmittReqMessage = "YOUR_REQUEST_IS_STILL_PENDING";
exports.productError = "PRODUCT_NOT_FOUND";
exports.otpMisMatch = "OTP_DID_NOT_MATCH";
exports.otpExpires = "INVALID_OTP_OR_OTP_EXPIRED";
exports.invalidUser = "INVALID_USER";
exports.enableOrder = "ENABLE_TO_ORDERED";
exports.googleError =
  "You have logged in with this email through google, so you can not create new password from here but you can login with email through google";
exports.facebookError =
  "You have logged in with this email through facebook, so you can not create new password from here but you can login with email through facebook";
