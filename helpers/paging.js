exports.pager = req=> {
    
        let params = req.query
        let pageNo=1
        let items=10
        let skipCount
        if ((params.pageNo != null && params.pageNo != undefined && params.pageNo != "") && (params.items != null && params.items != undefined && params.items != "")) {
            pageNo = (Number(params.pageNo))
            items = (Number(params.items))
            skipCount = items * (pageNo - 1)
        }
        return {
            pageNo: pageNo,
            items: items,
            skipCount: skipCount
        }
    
}