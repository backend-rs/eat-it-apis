'use strict'
const response = require('../exchange/response');
const message = require('../helpers/message');
const service = require('../services/ratings');
const { model } = require('mongoose');


const createTemporderObj = async (favorites, count, skipCount, totalCount) => {
    var favoritesObj = {
        favorites: favorites,
        skipCount: skipCount,
    }
    return favoritesObj;
}



const set = (model, entity, context) => {
    const log = context.logger.start('services/favorites/set')

    if (model.isfollowed) {
        entity.isfollowed = model.isfollowed
    }


    log.end()
    entity.save()
    return entity
}

const create = async (req, model, context, res) => {
    const log = context.logger.start('services/favorites')
    let favorite
    let isExists = false
    try {
        favorite = await new db.favorite(model).save();
        favorite.buyerId = context.user._id
        let order = await db.order.findById(model.orderId)
        if (order) {
            favorite.sellerId = order.sellerId

            // for order like
            if (model.type == 'like') {
                favorite.isfollowed = true
                let user = await db.user.findById(order.sellerId)
                let userTemp = await db.user.findById(context.user._id)

                if (userTemp) {

                    // push follower in user
                    if (userTemp.follower.length != 0) {
                        for (const item of userTemp.follower) {
                            if (item.id == order.sellerId) {
                                isExists = true
                            }
                        }
                    }

                    if (isExists == false) {
                        userTemp.follower.push({
                            id: order.sellerId
                        })
                        userTemp.save()
                        if (user) {
                            user.followerCount += 1
                            user.save()
                        }
                    }

                }
            }
            favorite.save()
        }
        if (model.ratingNo && model.sellerId) {
            // call rating create api
            let rating = await service.create(req, model, context, res)

        }
        log.end();
        return favorite
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
const update = async (id, model, context) => {
    const log = context.logger.start(`services/favorites:${id}`)
    try {

        const entity = id === 'my' ? context.favorite : await db.favorite.findById(id)
        if (model.isfollowed == 'false') {
            let user = await db.user.findById(model.buyerId)
            if (user) {
                for (let temp of user.follower) {
                    if (temp.id == model.sellerId) {


                        let index = user.follower.indexOf(temp)
                        console.log(index)
                        let removeItem = (user.follower).splice(index, 1)
                        console.log(removeItem)

                    }
                }
                user.save()
                // user.follower.splice(0,model.sellerId)
            }

        }

        if (!entity) {
            throw new Error(message.favoriteError);
        }

        // call set method to update favorite
        await set(model, entity, context)
        log.end()
        return entity
    } catch (err) {
        throw new Error(err)
    }
}

const get = async (req, page, context) => {
    const log = context.logger.start(`services/favorites/get`)
    try {
        let params = req.query
        let queryModel = {}
        let favorite
        let user
        let temp = []
        let userfollower

        if (params.status && (params.status != null && params.status != undefined && params.status != '')) {
            queryModel = {
                status: {
                    $eq: params.status
                }
            }
        }
        if (params.buyerId && (params.buyerId != null && params.buyerId != undefined && params.buyerId != '')) {
            queryModel = Object.assign({
                "buyerId": {
                    $eq: params.buyerId
                }
            }, queryModel)
        }
        if (params.sellerId && (params.sellerId != null && params.sellerId != undefined && params.sellerId != '')) {
            queryModel = Object.assign({
                "sellerId": {
                    $eq: params.sellerId
                }
            }, queryModel)
        }

        if (page != null && page != undefined && page != "") {
            favorite = await db.favorite.find(queryModel).skip(page.skipCount).limit(page.items).sort({
                timeStamp: -1
            })

            if (params.buyerId) {
                user = await db.user.findById(params.buyerId)

                if (user) {
                    for (const userTemp of user.follower) {
                        if (userTemp) {
                            userfollower = await db.user.findById(userTemp.id)

                        }
                        if (userfollower.id != null) {
                            temp.push({
                                // id: item._id,
                                // foodId: item.foodId,
                                sellerId: userfollower.id,
                                // status: item.status,
                                // isfollowed: item.isfollowed,
                                // type: item.type,
                                firstName: userfollower.firstName,
                                lastName: userfollower.lastName,
                                image: userfollower.image,
                                followerCount: userfollower.followerCount,
                                totalDishes: userfollower.totalDishes,
                                // follower: userfollower.follower

                            })
                        }
                    }
                }



            }
            const temporderResponseObj = await createTemporderObj(temp)
            favorite = temporderResponseObj
        }
        log.end();
        return favorite
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
exports.create = create
exports.update = update
exports.get = get
