"use strict";
const encrypt = require("../permit/crypto");
const auth = require("../permit/auth");
const randomize = require("randomatic");
const nodemailer = require("nodemailer");
const response = require("../exchange/response");
const message = require("../helpers/message");
const helpers = require("../helpers/mail-html");
const otp = require("../helpers/otp");
const moment = require("moment");

const createTempUserObj = async (user, skipCount, totalCount, totalPages) => {
  var userObj = {
    users: user,
    skipCount: skipCount,
    totalCount: totalCount,
    totalPages: totalPages,
  };

  return userObj;
};

const createTemporderObj = async (users, accountDetails) => {
  var userObj = {
    accountDetails: accountDetails,
    users: users,
  };
  return userObj;
};
const set = (model, entity, context) => {
  const log = context.logger.start("services/users/set");

  if (model.firstName) {
    entity.firstName = model.firstName;
  }
  if (model.lastName) {
    entity.lastName = model.lastName;
  }

  if (model.phone) {
    entity.phone = model.phone;
  }
  if (model.status) {
    if (context.user.type == "admin") {
      entity.status = model.status;
    }
  }
  console.log('mode', model.communicationMode)
  if (model.communicationMode) {
    entity.communicationMode = model.communicationMode;
  }
  if (model.firebaseToken) {
    entity.firebaseToken = model.firebaseToken;
  }
  if (model.address) {
    if (model.address.line) {
      entity.address.line = model.address.line;
    }

    if (model.address.pincode) {
      entity.address.pincode = model.address.pincode;
    }
    if (model.address.city) {
      entity.address.city = model.address.city;
    }
    if (model.address.country) {
      entity.address.country = model.address.country;
    }
    if (model.address.landmark) {
      entity.address.landmark = model.address.landmark;
    }
    if (model.address.location) {
      entity.address.location = model.address.location;
    }
  }
  if (model.image) {
    if (model.image.url) {
      entity.image.url = model.image.url;
    }
    if (model.image.thumbnail) {
      entity.image.thumbnail = model.image.thumbnail;
    }
    if (model.image.resize_url) {
      entity.image.resize_url = model.image.resize_url;
    }
    if (model.image.resize_thumbnail) {
      entity.image.resize_thumbnail = model.image.resize_thumbnail;
    }
  }
  log.end();
  // entity.save()
  return entity;
};

const create = async (req, model, context, res) => {
  const log = context.logger.start("services/users");
  try {
    let user;

    let query = {};

    if (model.email && model.loginType == "app") {
      query.email = model.email;
    }
    if (model.loginType) {
      query.loginType = model.loginType;
    }

    if (model.uId) {
      query.uId = model.uId;
    }
    // find user
    user = await db.user.findOne(query);
    if (!user) {
      if (model.loginType == "app") {
        if (model.email) {
          user = await db.user.findOne({
            email: {
              $eq: model.email,
            },
          });
          if (user) {
            log.end();
            return response.unprocessableEntity(res, "user_already_exists");
          }

          // encrypt password
          model.password = encrypt.getHash(model.password, context);

          // create user
          user = await new db.user(model).save();
          (user.totalDishes = 0), (user.followerCount = 0);
          user.save();
        }
      } else {
        // create user
        user = await new db.user(model).save();
        // generate token
        const token = auth.getToken(user.id, false, context);
        if (!token) {
          return response.unAuthorized(res, "token_error");
        }
        user.token = token;
        user.isVerified = true;
        user.status = "active";
        (user.totalDishes = 0), (user.followerCount = 0);

        user.save();
      }
    } else {
      if (user.loginType == "app") {
        log.end();
        return response.unprocessableEntity(res, "user_already_exists");
      }
    }

    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const update = async (id, model, context) => {
  const log = context.logger.start(`services/users:${id}`);
  try {
    const entity = id === "my" ? context.user : await db.user.findById(id);
    // call set method to update user
    console.log('modelmodelmodel',model)
    await set(model, entity, context);

    if (model.isfollowed == "false") {
      // let user = await db.user.findById(model.buyerId)
      if (entity) {
        for (let temp of entity.follower) {
          if (temp.id == model.sellerId) {
            let index = entity.follower.indexOf(temp);
            console.log(index);
            let removeItem = entity.follower.splice(index, 1);
            console.log(removeItem);
          }
        }

        // user.follower.splice(0,model.sellerId)
      }
    }

    if (!entity) {
      throw new Error(message.userError);
    }

    log.end();
    return entity.save();
  } catch (err) {
    throw new Error(err);
  }
};

const getById = async (id, context) => {
  const log = context.logger.start(`services/users/getById:${id}`);

  try {
    let user = id === "my" ? context.user : await db.user.findById(id);
    // if(user.accountDetailId){
    //   let accountDetails=await db.userAccount.find({
    //     userId: {
    //       $eq: id
    //     }
    //     });
    //   const tempcreateTemporderObj = await createTemporderObj(
    //     user,
    //     accountDetails
      
    //   );
    //   user = tempcreateTemporderObj;
    // }
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const get = async (req, page, context) => {
  const log = context.logger.start(`services/users`);
  try {
    let params = req.query;
    let queryModel = {};
    let users;
    let totalCount;
    let totalPages;
    if ((context.user.type = "admin")) {
      if (
        params.type &&
        (params.type != null ||
          params.type != undefined ||
          params.type != "") &&
        (params.pageNo != null ||
          params.pageNo != undefined ||
          params.pageNo != "") &&
        (params.items != null ||
          params.items != undefined ||
          params.items != "")
      ) {
        queryModel = {
          type: {
            $eq: params.type,
          },
        };
      }
      // if status
      if (
        params.status &&
        (params.status != null ||
          params.status != undefined ||
          params.status != "")
      ) {
        queryModel = Object.assign(
          {
            status: {
              $eq: params.status,
            },
          },
          queryModel
        );
      }
      if (page != null && page != undefined && page != "") {
        users = await db.user
          .find(queryModel)
          .skip(page.skipCount)
          .limit(page.items)
          .sort({
            timeStamp: -1,
          });
        let userTemp = await db.user.find({});
        totalCount = userTemp.length;
        totalPages = totalCount / page.items;
        const tempUserResponseObj = await createTempUserObj(
          users,
          page.skipCount,
          totalCount,
          totalPages
        );
        users = tempUserResponseObj;
      } else {
        users = await db.user.find(queryModel);
      }
    } else {
      throw response.unAuthorized(res, "unAuthorized_user");
    }
    log.end();
    return users;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const login = async (model, context) => {
  const log = context.logger.start(`services/users/login`);

  try {
    let user;
    const query = {};

    if (model.email) {
      query.email = model.email;
    }

    // find user
    user = await db.user.findOne(query);

    if (!user) {
      // user not found
      log.end();
      throw new Error(message.userError);
    } else {
      // match password
      const isMatched = encrypt.compareHash(
        model.password,
        user.password,
        context
      );
      if (!isMatched) {
        log.end();
        throw new Error(message.passwordError);
      }

      // create token
      const token = auth.getToken(user._id, false, context);
      if (!token) {
        throw new Error("token error");
      }
      user.token = token;
      user.status = "active";
      user.save();
    }
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const changePassword = async (model, context) => {
  const log = context.logger.start(`services/users/changePassword`);

  try {
    // find user
    const entity = await db.user.findOne({
      _id: {
        $eq: context.user.id,
      },
    });
    if (!entity) {
      throw new Error(message.userError);
    }

    // match old password
    const isMatched = encrypt.compareHash(
      model.password,
      entity.password,
      context
    );
    if (!isMatched) {
      throw new Error(message.compareOldPassword);
    }

    // update & encrypt password
    entity.password = encrypt.getHash(model.newPassword, context);

    log.end();
    return entity.save();
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const forgotPassword = async (model, context) => {
  const log = context.logger.start(`services/users/forgotPassword`);

  try {
    let user;
    if (model.email) {
      // find user
      user = await db.user.findOne({
        email: {
          $eq: model.email,
        },
      });
    }
    if (!user) {
      throw new Error(message.userError);
    }
    let loginType = user.loginType;
    if (loginType == "google") {
      throw new Error(message.googleError);
    }
    if (loginType == "facebook") {
      throw new Error(message.facebookError);
    }
    // call send otp function
    await otp.sendOtp(model, user, context);
    const date = new Date();
    const expiryTime = moment(date.setMinutes(date.getMinutes() + 30));

    user.expiryTime = expiryTime;
    user.save();

    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const resetPassword = async (model, context) => {
  const log = context.logger.start(`services/users/verifyOtp`);

  try {
    let user;
    // find user
    user = await db.user.findOne({
      email: {
        $eq: model.email,
      },
    });
    if (!user) {
      throw new Error(message.userError);
    }

    // call matchOtp method
    await otp.matchOtp(model, user, context);

    // update password
    if (user) {
      user.password = encrypt.getHash(model.newPassword, context);
      user.save();
    }
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const logOut = async (res, context) => {
  const log = context.logger.start(`services/users/logOut`);
  try {
    const user = await db.user.findOne({
      _id: {
        $eq: context.user.id,
      },
    });
    if (!user) {
      throw new Error(message.userError);
    }
    user.token = "";
    user.save();
    res.message = message.logOutMessage;
    log.end();
    return response.data(res, "");
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const getSocketUsers = async (req, context) => {
  const log = context.logger.start(`services/users/getSocketUsers`);
  try {
    let params = req.query;
    let user;
    let orders;
    let userStack = [];
    let tempMsg;
    let temptime;
    let isSms = false;
    let isPhone = false;
    let isExpired = false;
    let chatListuser;
    let chatUserId;
    let userModel = {};
    if (params.userId) {
      // find user itself
      user = await db.user.findById(params.userId);
      if (!user) {
        throw new Error("User not found");
      }
      // find all orders of user as a seller and buyer
      orders = await db.order.find({
        $or: [{ buyerId: params.userId }, { sellerId: params.userId }],
        status: {
          $in: ["delivered", "confirmed"],
        },
      });

      if (orders && orders.length != 0) {
        // for each order
        for (let order of orders) {
          if (order) {
            // if user is a buyer
            if (order.buyerId == params.userId) {
              chatUserId = order.sellerId;
            }
            // if user is a seller
            if (order.sellerId == params.userId) {
              chatUserId = order.buyerId;
            }
            chatListuser = await db.user.findById(chatUserId);
            if (!chatListuser) {
              throw new Error("chatListuser not found");
            }
            // find last chat
            let chat = await db.chats
              .findOne({
                orderId: {
                  $eq: order.orderId,
                },
              })
              .sort({ _id: -1 })
              .limit(1);

            if (!chat) {
              tempMsg = "";
              temptime = "";
            } else {
              tempMsg = chat.msg;
              temptime = chat.createdOn;
            }
            if (
              user.communicationMode == "phone" &&
              chatListuser.communicationMode == "phone"
            ) {
              isPhone = true;
            }
            if (
              user.communicationMode == "sms" &&
              chatListuser.communicationMode == "sms"
            ) {
              isSms = true;
            }
            if (
              user.communicationMode == "both" &&
              chatListuser.communicationMode == "both"
            ) {
              isSms = true;
              isPhone = true;
            }
            if (
              user.communicationMode == "none" &&
              chatListuser.communicationMode == "none"
            ) {
              isSms = false;
              isPhone = false;
            }

            let food = await db.food.findById(order.foodId);
            console.log('order.foodId)order.foodId)order.foodId)',order.foodId)
            if (!food) {
              // throw new Error("invalid food");
              continue;
            }
            if (food.availabilityTime == 0) {
              isExpired = true;
            } else {
              isExpired = false;
            }
            userModel = {
              msg: tempMsg,
              time: temptime,
              isPhone: isPhone,
              isSms: isSms,
              username: chatListuser.firstName,
              lastname: chatListuser.lastName,
              status: order.status,
              communicationMode: chatListuser.communicationMode,
              phone: chatListuser.phone,
              userId: chatListuser._id,
              orderId: order.orderId,
              isExpired: isExpired,
              timeStamp: order.timeStamp,
            };
            userStack.push(userModel);
          }
        }
        let tempSort = userStack.sort(function (a, b) {
          return new Date(b.timeStamp) - new Date(a.timeStamp);
        });
        user = tempSort;
      } else {
        user = userStack;
      }
    }
    log.end();
    return user;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};
const addField = async () => {
  try {
    let user = await db.user.update(
      {},
      { $set: { paymentId: "1" } },
      { upsert: false, multi: true }
    );
    return user;
  } catch (err) {
    throw new Error(err);
  }
};
exports.create = create;
exports.getById = getById;
exports.login = login;
exports.forgotPassword = forgotPassword;
exports.resetPassword = resetPassword;
exports.changePassword = changePassword;
exports.logOut = logOut;
exports.update = update;
exports.get = get;
exports.getSocketUsers = getSocketUsers;
exports.addField = addField;
