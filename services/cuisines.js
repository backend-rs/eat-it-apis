'use strict'
const response = require('../exchange/response');
const message = require('../helpers/message');

const set = (model, entity, context) => {
    const log = context.logger.start('services/users/set')

    if (model.name) {
        entity.name = model.name
    }
    if (model.status) {
        entity.status = model.status
    }

    if (model.image) {
        if (model.image.url) {
            entity.image.url = model.image.url
        }
        if (model.image.thumbnail) {
            entity.image.thumbnail = model.image.thumbnail
        }
        if (model.image.resize_url) {
            entity.image.resize_url = model.image.resize_url
        }
        if (model.image.resize_thumbnail) {
            entity.image.resize_thumbnail = model.image.resize_thumbnail
        }
    }
    log.end()
    // entity.save()
    return entity
}

const create = async (req, model, context, res) => {
    const log = context.logger.start('services/cuisines')
    try {
        let cuisine
        if(context.user.type=='admin'){
            cuisine = await new db.cuisine(model).save();
        }else{
            log.end()
            throw new Error(message.invalidUser)
        }
        log.end();
        return cuisine
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}



const update = async (id, model, context) => {
    const log = context.logger.start(`services/cuisine:${id}`)
    try {

        const entity = id === 'my' ? context.user : await db.cuisine.findById(id)
        // call set method to update user
        await set(model, entity, context)


        if (!entity) {
            throw new Error(message.userError);
        }


        log.end()
        return entity.save()
    } catch (err) {
        throw new Error(err)
    }
}






const getById = async (id, context) => {
    const log = context.logger.start(`services/users/getById:${id}`)

    try {
        // find food
        let cuisine = await db.cuisine.findById(id)

        log.end()
        return cuisine

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, page, context) => {
    const log = context.logger.start(`services/cuisines/get`)
    try {
        let params = req.query
        let query = {}
        let cuisine
        
        if (params.status && (params.status != null && params.status != undefined && params.status != '')) {
            query.status = params.status
        } 
        if (page != null && page != undefined && page != "") {
            cuisine = await db.cuisine.find(query).skip(page.skipCount).limit(page.items).sort({
                timeStamp: -1  
            })
        }else{
            cuisine = await db.cuisine.find(query)
        }
        
        log.end();
        return cuisine
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
exports.create = create
exports.update=update

exports.get = get
exports.getById=getById