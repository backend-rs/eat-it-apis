"use strict";
const moment = require("moment");
const service = require("./pushNotification");
const message = require("../helpers/message");
const cuisines = require("./cuisines");
// const Geo = require('geo-nearby');
const milesToRadian = function (miles) {
  var earthRadiusInMiles = 3959;
  return miles / earthRadiusInMiles;
};
const set = (model, entity) => {
   if (model.isAdvanceFoodPost != null && model.isAdvanceFoodPost != undefined ) {
    entity.isAdvanceFoodPost = model.isAdvanceFoodPost;
    if (model.advanceFoodDate != null && model.advanceFoodDate != undefined ) {
      entity.advanceFoodDate = model.advanceFoodDate;
    } else {
      throw new Error('Advance Food Date is compulsory');
    }
  }
  if (model.status != '' && model.status != null) {
    entity.status = model.status;
  }
  if (model.name != undefined && model.name != '' && model.name != 'string') {
    entity.name = model.name;
  }
  if (model.type != undefined && model.type != '' && model.type != 'string') {
    entity.type = model.type;
  }
  if (model.foodCooked != undefined && model.foodCooked != '' && model.foodCooked != 'string') {
    entity.foodCooked = model.foodCooked;
  }
  if (model.homeDelivery != undefined && model.homeDelivery != '' && model.homeDelivery != 'string') {
    entity.homeDelivery = model.homeDelivery;
    if (model.homeDeliveryPrice != undefined && model.homeDeliveryPrice != '' && model.homeDeliveryPrice != 'string') {
      entity.homeDeliveryPrice = model.homeDeliveryPrice || 20;
    }
  }
  if (model.quantity != undefined && model.quantity != '' && model.quantity != 'string') {
    entity.quantity = model.quantity;
  }
  if (model.description != undefined && model.description != '' && model.description != 'string') {
    entity.description = model.description;
  }
  if (model.availabilityTime != undefined && model.availabilityTime != '' && model.availabilityTime != 'string') {
    entity.availabilityTime = model.availabilityTime;
  }
  if (model.pickupTime != undefined && model.pickupTime.from != undefined && model.pickupTime.from != '' &&model.pickupTime.from != 'string') {
    entity.pickupTime.from = model.pickupTime.from
  }
  if (model.pickupTime != undefined && model.pickupTime.to != undefined && model.pickupTime.to != '' &&model.pickupTime.to != 'string') {
    entity.pickupTime.to = model.pickupTime.to
  }
  if (model.price != undefined &&  model.price != '' &&model.price != 'string') {
    entity.price = model.price
  }
  if (model.address != undefined) {
    entity.address = model.address
  }
  entity.save();
  return entity;
};
const create = async (req, model, context, res) => {
  const log = context.logger.start("services/foods");
  try {
    let food;
    let user;
    let lastName;
    let firstName;
    // add food in db
    food = await new db.food(model).save();

    // update seller dishes
    user = await db.user.findById(context.user.id);
    if (user) {
      user.totalDishes += 1;
      user.save();
    }
    food.userId = context.user.id;
    //find  user following
    let users = await db.user.find({
      follower: { $elemMatch: { id: context.user.id } },
    });
    if (users.length != 0) {
      for (const tempUser of users) {
        if (user.firstName == null || user.firstName == undefined) {
          firstName = "";
        }
        if (user.lastName == null || user.lastName == undefined) {
          lastName = "";
        }
        if (user.firstName && user.lastName) {
          lastName = user.lastName
            .toLowerCase()
            .split(" ")
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join(" ");
          firstName = user.firstName
            .toLowerCase()
            .split(" ")
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join(" ");
        }

        // send notification
        let temp = {
          notification: {
            title: "New food",
            body: `New food added by ${firstName} ${lastName} `,
          },
          data: {
            notificationType: "New food",
            title: "New food",
            body: `New food added by ${firstName} ${lastName} `,
            id: `${food._id}`,
          },
          to: `${tempUser._id}`,
          foodId: `${food._id}`,
        };
        await service.create("", temp, context, "");
      }
    }

    food.save();
    log.end();

    return food;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const getById = async (id, context) => {
  const log = context.logger.start(`services/users/getById:${id}`);

  try {
    // find food
    let cuisineData = []
    let food = await db.food.findById(id)

    if(food) {
        let data = await db.cuisine.find()
        food.cuisine.forEach(fcuisine => {
          data.forEach(cuisine => {
            if(fcuisine.id == cuisine.id) {
              cuisineData.push({id:cuisine.id, name:cuisine.name})
            }
          });
        });
       
        // console.log('data', data)
    }
    let rData = {
      food: food,
      cuisineData: cuisineData
    }
    log.end();
    return rData;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const get = async (req, page, context) => {
  const log = context.logger.start(`services/foods/get`);
  try {
    let foods;
    let query = {};
    let params = req.query;
    let now = moment(new Date());
    let start;
    let end;
    start = now.clone().startOf("day");
    end = now.clone().endOf("day");

    // search by type
    if (
      params.userId &&
      params.userId != null &&
      params.userId != undefined &&
      params.userId != ""
    ) {
      console.log('djdadh111')
      foods = await db.food.find({
        userId: {
          $eq: params.userId,
        },
      });
    } else {
      // add paging
      if (
        params.page != null &&
        params.page != undefined &&
        params.page != ""
      ) {
        foods = await db.food
          .find(
            {
              // quantity: {
              //   $ne: 0,
              // },
              status: {
                $eq: "active",
              },
            },
            query
          )
          .skip(page.skipCount)
          .limit(page.items)
          .sort({
            timeStamp: -1,
          });
      } else {
        foods = await db.food.find(
          {
            // quantity: {
            //   $ne: 0,
            // },
            status: {
              $eq: "active",
            },
          },
          query
        );
      }
    }
    log.end();
    return foods;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const search = async (req, page, context) => {
  const log = context.logger.start(`services/foods/search`);
  console.log(`services/foods/search..`, req.query)
  try {
    let params = req.query;
    let queryModel = {};
    let foods;
    let name;
    let temp = [];
    let now = moment(new Date());
    let start;
    let end;
    let country;
    let hasCountry = false;
    start = now.clone().startOf("day");
    end = now.clone().endOf("day");
    if (params.getByCountry != null && params.getByCountry != 'null' &&
      params.getByCountry != undefined &&
      params.getByCountry != "" && params.getByCountry != 'false') {
        console.log('paramsparamsparams',params)
      country = params.getByCountry
      hasCountry = true
    }
    if (
      params.cuisineName != null &&
      params.cuisineName != undefined &&
      params.cuisineName != ""
    ) {
      queryModel = {
        name: {
          $eq: params.cuisineName,
        },
      };
      let cuisine = await db.cuisine.findOne(queryModel);

      let query = {
        cuisine: { $elemMatch: { id: cuisine.id } },
        // "timeStamp": {
        //     $gte: start,
        //     $lte: end
        // },
        status: {
          $eq: "active",
        },
      };
      if (hasCountry) {
        query['address.country'] = country
      }
      console.log('queryquery1', query)
      foods = await db.food
        .find(query)
        .skip(page.skipCount)
        .limit(page.items)
        .sort({
          timeStamp: -1,
        });
      return foods;
    }
    if (params.name != null && params.name != undefined && params.name != "") {
      name = params.name.toLowerCase() || "";
    }
    // search by food name
    if (
      params.name &&
      params.searchType == "food" &&
      params.name != null &&
      params.name != undefined &&
      params.name != ""
    ) {
      queryModel = {
        name: {
          $regex: name,
          $options: "i",
        },
        // timeStamp: {
        //     $gte: start,
        //     $lte: end
        // },
        status: {
          $eq: "active",
        },

        // quantity: {
        //   $ne: 0,
        // }
      };
    }

    // search by cuisine name(category)
    if (
      params.name &&
      params.searchType == "cuisine" &&
      params.name != null &&
      params.name != undefined &&
      params.name != ""
    ) {
      queryModel = {
        name: {
          $regex: name,
          $options: "i",
        },
      };
      foods = await db.cuisine
        .find(queryModel)
        .skip(page.skipCount)
        .limit(page.items)
        .sort({
          timeStamp: -1,
        });
    }

    // search by cuisine
    if (
      params.cuisineId &&
      params.cuisineId != null &&
      params.cuisineId != undefined &&
      params.cuisineId != ""
    ) {
      queryModel = Object.assign(
        {
          cuisine: { $elemMatch: { id: params.cuisineId } },
          // "timeStamp": {
          //     $gte: start,
          //     $lte: end
          // },
          status: {
            $eq: "active",
          },
        },
        queryModel
      );
    }

    // search by type
    if (
      params.type &&
      params.type != null &&
      params.type != undefined &&
      params.type != ""
    ) {
      queryModel = Object.assign(
        {
          type: {
            $eq: params.type,
          },
          // "timeStamp": {
          //     $gte: start,
          //     $lte: end
          // },
          status: {
            $eq: "active",
          },
        },
        queryModel
      );
    }
    // search by type
    if (
      params.isFoodFree &&
      params.searchType == "food" &&
      params.isFoodFree != null &&
      params.isFoodFree != undefined &&
      params.isFoodFree != ""
    ) {
      queryModel = Object.assign(
        {
          type: {
            $ne: "langar",
          },
          price: {
            $eq: 0,
          },
          // quantity: {
          //   $ne: 0,
          // },
          status: {
            $eq: "active",
          },
        },
        queryModel
      );
    }

    // search by cost
    if (
      params.cost &&
      params.cost == "paid" &&
      params.cost != null &&
      params.cost != undefined &&
      params.cost != ""
    ) {
      if (
        params.minPrice &&
        params.maxPrice == null &&
        params.minPrice != null &&
        params.minPrice != undefined &&
        params.minPrice != ""
      ) {
        queryModel = Object.assign(
          {
            price: {
              $gte: parseInt(params.minPrice),
            },
            status: {
              $eq: "active",
            },
          },
          queryModel
        );
      }
      if (
        params.maxPrice &&
        params.maxPrice != null &&
        params.maxPrice != undefined &&
        params.maxPrice != "" &&
        (params.minPrice == null ||
          params.minPrice == undefined ||
          params.minPrice == "")
      ) {
        queryModel = Object.assign(
          {
            price: {
              $lte: parseInt(params.maxPrice),
            },
            status: {
              $eq: "active",
            },
          },
          queryModel
        );
      }
      if (
        params.maxPrice &&
        params.minPrice &&
        params.maxPrice != null &&
        params.maxPrice != undefined &&
        params.maxPrice != ""
      ) {
        queryModel = Object.assign(
          {
            price: {
              $lte: parseInt(params.maxPrice),
              $gte: parseInt(params.minPrice),
            },
            status: {
              $eq: "active",
            },
          },
          queryModel
        );
      }
    }
    // search by free food
    if (
      params.cost &&
      params.cost == "free" &&
      params.cost != null &&
      params.cost != undefined &&
      params.cost != ""
    ) {
      queryModel = Object.assign(
        {
          price: {
            $eq: 0,
          },
          status: {
            $eq: "active",
          },
          // quantity: {
          //   $ne: 0,
          // }
        },
        queryModel
      );
    }
    if (
      params.foodCooked &&
      params.foodCooked != null &&
      params.foodCooked != undefined &&
      params.foodCooked != ""
    ) {
      queryModel = Object.assign(
        {
          foodCooked: {
            $eq: params.foodCooked,
          },
          status: {
            $eq: "active",
          },
        },
        queryModel
      );
    }
    // search by location
    if (
      params.longitude &&
      params.latitude &&
      params.longitude != null &&
      params.longitude != undefined &&
      params.longitude != "" &&
      params.latitude != null &&
      params.latitude != undefined &&
      params.latitude != ""
    ) {
      let coordinates = [
        JSON.parse(params.longitude),
        JSON.parse(params.latitude),
      ];
      queryModel = Object.assign(
        {
          "address.location": {
            $geoWithin: {
              $centerSphere: [coordinates, milesToRadian(2)],
            },
          },
          // "timeStamp": {
          //     $gte: start,
          //     $lte: end
          // },
          status: {
            $eq: "active",
          },
          // quantity: {
          //   $ne: 0,
          // }
        },
        queryModel
      );
    }
    // search by location
    if (
      params.longitude &&
      params.latitude &&
      params.miles &&
      params.longitude != null &&
      params.longitude != undefined &&
      params.longitude != "" &&
      params.latitude != null &&
      params.latitude != undefined &&
      params.latitude != ""
    ) {
      let coordinates = [
        JSON.parse(params.longitude),
        JSON.parse(params.latitude),
      ];
      queryModel = Object.assign(
        {
          "address.location": {
            $geoWithin: {
              $centerSphere: [coordinates, milesToRadian(params.miles)],
            },
          },
          // "timeStamp": {
          //     $gte: start,
          //     $lte: end
          // },
          status: {
            $eq: "active",
          },
          // quantity: {
          //   $ne: 0,
          // }
        },
        queryModel
      );
    }
    // search by food miles
    if (
      params.miles &&
      params.userId &&
      params.miles != null &&
      params.miles != undefined &&
      params.miles != ""
    ) {
      let temp = await db.user.findById(params.userId);

      let address = temp.address;

      queryModel = Object.assign(
        {
          "address.location": {
            $geoWithin: {
              $centerSphere: [
                address.location.coordinates,
                milesToRadian(params.miles),
              ],
            },
          },
          // "timeStamp": {
          //     $gte: start,
          //     $lte: end
          // },
          // quantity: {
          //   $ne: 0,
          // },
          status: {
            $eq: "active",
          },
        },
        queryModel
      );
      // let foodTemp = await db.food.find(query).sort({
      //     timeStamp: -1
      // })
      // return foodTemp
    }
    // add paging
    if (
      params.searchType == "food" &&
      page != null &&
      page != undefined &&
      page != ""
    ) {
      foods = await db.food
        .find(queryModel)
        .skip(page.skipCount)
        .limit(page.items)
        .sort({
          timeStamp: -1,
        });
    } else if (
      params.userId &&
      params.userId != null &&
      params.userId != undefined &&
      params.userId != ""
    ) {
      if (
        params.isfollow &&
        params.isfollow == "true" &&
        params.isfollow != null &&
        params.isfollow != undefined &&
        params.isfollow != ""
      ) {
        let users = await db.user.findById(params.userId);
        if (users) {
          if (users.follower.length != 0) {
            for (const user of users.follower) {
              if (user) {
                let query = {
                  userId: {
                    $eq: user.id,
                  },
                  // "timeStamp": {
                  //     $gte: start,
                  //     $lte: end
                  // },
                  status: {
                    $eq: "active",
                  }
                }
                if (hasCountry) {
                  query['address.country'] = country
                }
                console.log('queryquery2', query)
                let foodItem = await db.food
                  .find(query)
                  .skip(page.skipCount)
                  .limit(page.items);
                if (foodItem.length != 0) {
                  for (const item of foodItem) {
                    if (item) {
                      temp.push({
                        id: item._id,
                        name: item.name,
                        youWant: item.youWant,
                        type: item.type,
                        homeDelivery: item.homeDelivery,
                        homeDeliveryPrice: item.homeDeliveryPrice,
                        quantity: item.quantity,
                        description: item.description,
                        availabilityTime: item.availabilityTime,
                        status: item.status,
                        pickupTime: item.pickupTime,
                        price: item.price,
                        userId: item.userId,
                        images: item.images,
                        foodCooked: item.foodCooked,
                        cuisine: item.cuisine,
                        address: item.address,
                      });
                    }
                  }
                }
              }
            }
          }
        }

        return temp;
      }
      if (
        params.forYou &&
        params.forYou == "true" &&
        params.forYou != null &&
        params.forYou != undefined &&
        params.forYou != ""
      ) {
        let orders = await db.order.distinct("sellerId", {
          buyerId: params.userId,
        });
        console.log("orderssss::", orders);
        let query = {
          userId: {
            $in: orders,
          },
          // "timeStamp": {
          //     $gte: start,
          //     $lte: end
          // },
          status: {
            $eq: "active",
          }
        }
        if (hasCountry) {
          query['address.country'] = country
        }
        console.log('queryquery3', query)
        let foodsTemp = await db.food
          .find(query)
          .skip(page.skipCount)
          .limit(page.items)
          .sort({
            timeStamp: -1,
          });
        return foodsTemp;
      }
      // search by food location
      if (
        params.nearByYou &&
        params.userId &&
        params.nearByYou == "true" &&
        params.nearByYou != null &&
        params.nearByYou != undefined &&
        params.nearByYou != ""
      ) {
        let temp = await db.user.findById(params.userId);

        let address = temp.address;
        console.log('address.locationaddress.location', address)
        let query = {}
        if (hasCountry) {
          query['address.country'] = country
        } else {
          query = {
            "address.location": {
              $geoWithin: {
                $centerSphere: [address.location.coordinates, milesToRadian(2)],
              },
            },
            // "timeStamp": {
            //     $gte: start,
            //     $lte: end
            // },
            status: {
              $eq: "active",
            },
          };
        }

        console.log('queryquery4', query, hasCountry, country)
        let foodTemp = await db.food
          .find(query)
          .skip(page.skipCount)
          .limit(page.items)
          .sort({
            timeStamp: -1,
          });
        return foodTemp;
      }
    } else {
      if (hasCountry) {
        queryModel['address.country'] = country
      }
      if (params.searchType == "food") {
        console.log('queryquery5', queryModel)
        foods = await db.food
          .find(queryModel)
          .skip(page.skipCount)
          .limit(page.items)
          .sort({
            timeStamp: -1,
          });
      }
    }

    log.end();
    return foods;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const update = async (id, model,context) => {
  const log = context.logger.start('api/foods/update')
  try {
    const entity = await db.food.findById(id);
    await set(model, entity);
    if (!entity) {
      throw new Error(message.userError);
    }
    log.end();
    return entity;
  } catch (err) {
    throw new Error(err);
  }
};

exports.create = create;
exports.get = get;
exports.getById = getById;
exports.search = search;
exports.update = update;
