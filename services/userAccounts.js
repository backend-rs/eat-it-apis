'use strict'
const response = require('../exchange/response');
const message = require('../helpers/message');






const set = (model, entity, context) => {
    const log = context.logger.start('services/favorites/set')

    if (model.accountDetail.accountNo) {
        entity.accountDetail.accountNo = model.accountDetail.accountNo
    }
    if (model.accountDetail.accountHolderName) {
        entity.accountDetail.accountHolderName = model.accountDetail.accountHolderName
    }
    if (model.accountDetail.ifscCode) {
        entity.accountDetail.ifscCode = model.accountDetail.ifscCode
    } if (model.accountDetail.bankName) {
        entity.accountDetail.bankName = model.accountDetail.bankName
    }

    
    log.end()
    entity.save()
    return entity
}

const create = async (req, model, context, res) => {
    const log = context.logger.start('services/userAccounts')
    let userAccount
    try {
     let   queryModel = {
            userId: {
                $eq: model.userId,
            },
        };
        userAccount = await db.userAccount.findOne(queryModel)
        if(!userAccount){
        let user = await db.user.findById(model.userId);
        
            userAccount = await new db.userAccount(model).save();

            user.accountDetailId = userAccount.id
            user.accountAdded = 'true'
        
      
        user.save()

        }else{
             await set(model,userAccount , context)
        }

        log.end();
        return userAccount
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, page, context) => {
    const log = context.logger.start(`services/userAccounts/get`)
    try {
        let params = req.query
        let queryModel = {}
        let userAccount
        if (params.userId && (params.userId != null || params.userId != undefined || params.userId != "")) {
            queryModel = {
                userId: {
                    $eq: params.userId,
                },
            };
        }

        if (page != null && page != undefined && page != "") {
            userAccount = await db.userAccount.find(queryModel).skip(page.skipCount).limit(page.items).sort({
                timeStamp: -1
            })
        } else {
            userAccount = await db.userAccount.find(queryModel)
        }

        log.end();
        return userAccount
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
exports.create = create

exports.get = get
