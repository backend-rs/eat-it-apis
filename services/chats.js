
const get = async (req, page, context) => {
  const log = context.logger.start("services/chats")
  try {
    let params = req.query
    let query = {}
    let chats
    let oldChats = []

    if (params.room && (params.room != null && params.room != undefined && params.room != '')) {
      query = {
        room: {
          $eq: params.room,
        },
      }
    }

    chats = await db.chats.find(query).sort({
      createdOn: -1
    })

    if (oldChats.length != 0) {
      oldChats = [];
    }
    chats.forEach((chat) => {
      let chatDetial = {
        chatId: chat._id,
        msgFrom: chat.msgFrom,
        msgTo: chat.msgTo,
        msg: chat.msg,
        room: chat.room,
        date: chat.createdOn,
      };
      oldChats.push(chatDetial);
    });
    log.end();
    return oldChats
  } catch (err) {
    log.end()
    throw new Error(err)
  }
}

exports.get = get
