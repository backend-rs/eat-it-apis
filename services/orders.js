"use strict";
const response = require("../exchange/response");
const message = require("../helpers/message");
const randomize = require("randomatic");
const service = require("./pushNotification");
// const { orderQuantity } = require("../models/order");

const createTemporderObj = async (orders, count, skipCount, totalCount) => {
  var orderObj = {
    orders: orders,
    skipCount: skipCount,
  };
  return orderObj;
};

const createTemordersObj = async (order, tempfavorite, food) => {
  var orderObj = {
    id: order._id,
    foodId: food.id,
    sellerId: order.sellerId,
    status: order.status,
    orderId: order.orderId,
    otp: order.otp,
    name: food.name,
    youWant: food.youWant,
    type: food.type,
    favoriteType: tempfavorite.type,
    favoriteStatus: tempfavorite.status,
    homeDelivery: food.homeDelivery,
    homeDeliveryPrice: food.homeDeliveryPrice,
    // quantity: food.quantity,
    description: food.description,
    availabilityTime: food.availabilityTime,
    foodStatus: food.status,
    pickupTime: food.pickupTime,
    price: food.price,
    userId: food.userId,
    images: food.images,

    foodCooked: food.foodCooked,
    cuisine: food.cuisine,
    address: food.address,
  };
  if (food.quantity) {
    orderObj.quantity = food.quantity;
  }
  if (order.orderQuantity) {
    orderObj.orderQuantity = order.orderQuantity;
  }

  return orderObj;
};

const set = (model, entity, context) => {
  const log = context.logger.start("services/orders/set");

  if (model.status) {
    entity.status = model.status;
  }

  log.end();
  entity;
  return entity;
};

const create = async (req, model, context, res) => {
  const log = context.logger.start("services/orders");
  try {
    let order;
    let payment;
    // find food
    let food = await db.food.findById(model.foodId);

    if (context.user.id != food.userId) {
      if (model.from && model.from == "paid") {
        if (model.paymentId) {
          payment = await db.payment.findOne({
            paymentId: {
              $eq: model.paymentId,
            },
          });
        }
        if (!payment) {
          throw new Error("PAYMENT_DOES_NOT_EXISTS");
        } else {
          model.paymentId = payment.id;
        }
      }
      // save order in db
      order = await new db.order(model).save();
      order.buyerId = context.user.id;
      order.sellerId = food.userId;
      if (food.type != "langar") {
        food.quantity = food.quantity - model.orderQuantity;
      }
      food.save();
      // order.total = model.orderQuantity * food.price;
      // ganerate orderId
      order.orderId = randomize("0", 7);
      order.save();

      // send notification to seller
      if (order.orderId == null || order.orderId == undefined) {
        order.orderId = "";
      }
      let temp = {
        notification: {
          title: "Order received",
          body: `Order no ${order.orderId} has been received`,
        },
        data: {
          notificationType: "Order received",
          title: "Order received",
          body: `Order no #${order.orderId} has been received`,
          id: `${order._id}`,
        },
        to: `${food.userId}`,
        orderCode: `${order.orderId}`,
        orderId: `${order._id}`,
      };
      await service.create("", temp, context, "");
    } else {
      log.end();
      throw new Error(message.userError);
    }
    log.end();
    return order;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const update = async (id, model, context) => {
  const log = context.logger.start(`services/orders:${id}`);
  try {
    const entity = id === "my" ? context.order : await db.order.findById(id);

    if (!entity) {
      throw new Error(message.orderError);
    }
    // call set method to update order
    await set(model, entity, context);

    // send notification for confirmed order
    if (model.status == "confirmed") {
      let food = await db.food.findById(entity.foodId);
      if (food.quantity > 0) {
        let foodquantity = food.quantity;
        food.quantity = foodquantity - 1;
        food.save();
      }
      if (entity.orderId == null || entity.orderId == undefined) {
        entity.orderId = "";
      }
      let temp = {
        notification: {
          title: "Order confirmed",
          body: `Order no #${entity.orderId} has been confirmed`,
        },
        data: {
          notificationType: "Order comfirmed",
          title: "Order confirmed",
          body: `Order no #${entity.orderId} has been confirmed`,
          id: `${entity._id}`,
        },
        orderCode: `${entity.orderId}`,
        to: `${entity.buyerId}`,
        orderId: `${entity._id}`,
      };
      await service.create("", temp, context, "");
      // generate otp
      const otp = randomize("0", 4);
      entity.otp = otp;
    }
    // send notification for delivered order
    if (model.otp) {
      if (model.otp == entity.otp) {
        if (entity.orderId == null || entity.orderId == undefined) {
          entity.orderId = "";
        }
        let temp = {
          notification: {
            title: "Order delivered",
            body: `Order no #${entity.orderId} has been delivered`,
          },
          data: {
            notificationType: "Order delivered",
            title: "Order delivered",
            body: `Order no #${entity.orderId} has been delivered`,
            id: `${entity._id}`,
          },
          orderCode: `${entity.orderId}`,
          to: `${entity.buyerId}`,
          orderId: `${entity._id}`,
        };
        await service.create("", temp, context, "");
        entity.status = "delivered";
      } else {
        throw new Error(message.otpMisMatch);
      }
    }

    log.end();
    return entity.save();
  } catch (err) {
    throw new Error(err);
  }
};

const getById = async (id, context) => {
  const log = context.logger.start(`services/orders/getById:${id}`);

  try {
    let food;
    let order;
    let tempfavorite = {};
    let tempType;
    let tempStatus;
    let favorite;

    // find order
    order = await db.order.findById(id);

    food = await db.food.findOne({
      _id: {
        $eq: order.foodId,
      },
    });
    favorite = await db.favorite.findOne({
      buyerId: {
        $eq: order.buyerId,
      },
      orderId: {
        $eq: order.id,
      },
    });
    if (favorite) {
      tempType = favorite.type;
      tempStatus = favorite.status;
    } else {
      tempType = "none";
      tempStatus = "";
    }
    tempfavorite = {
      type: tempType,
      status: tempStatus,
    };
    const temporderResponseObj = await createTemordersObj(
      order,
      tempfavorite,
      food
    );
    order = temporderResponseObj;
    log.end();
    return temporderResponseObj;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const get = async (req, page, context) => {
  const log = context.logger.start(`services/orders/get`);
  try {
    let params = req.query;
    let queryModel = {};
    let temp = [];
    let order;
    let food;
    let tempfavorite = {};
    let tempType;
    let tempStatus;
    let favorite;

    if (
      params.status &&
      params.status != null &&
      params.status != undefined &&
      params.status != ""
    ) {
      queryModel = {
        status: {
          $eq: params.status,
        },
      };
    }
    if (
      params.buyerId &&
      params.buyerId != null &&
      params.buyerId != undefined &&
      params.buyerId != ""
    ) {
      queryModel = Object.assign(
        {
          buyerId: {
            $eq: params.buyerId,
          },
        },
        queryModel
      );
    }
    if (
      params.sellerId &&
      params.sellerId != null &&
      params.sellerId != undefined &&
      params.sellerId != ""
    ) {
      queryModel = Object.assign(
        {
          sellerId: {
            $eq: params.sellerId,
          },
        },
        queryModel
      );
    }
    if (
      params.history == "true" &&
      params.history != null &&
      params.history != undefined &&
      params.history != ""
    ) {
      queryModel = Object.assign(
        {
          status: {
            $in: ["delivered", "rejected"],
          },
        },
        queryModel
      );
    }
    if (
      params.history == "false" &&
      params.history != null &&
      params.history != undefined &&
      params.history != ""
    ) {
      queryModel = Object.assign(
        {
          status: {
            $in: ["pending", "confirmed"],
          },
        },
        queryModel
      );
    }
    if (page != null && page != undefined && page != "") {
      order = await db.order
        .find(queryModel)
        .skip(page.skipCount)
        .limit(page.items)
        .sort({
          timeStamp: -1,
        });
    }
    if (order.length != 0) {
      for (const item of order) {
        if (item.foodId) {
          food = await db.food.findById(item.foodId);
        }
        favorite = await db.favorite.findOne({
          buyerId: {
            $eq: item.buyerId,
          },
          orderId: {
            $eq: item.id,
          },
        });
        if (favorite) {
          tempType = favorite.type;
          tempStatus = favorite.status;
        } else {
          tempType = "none";
          tempStatus = "";
        }
        tempfavorite = {
          type: tempType,
          status: tempStatus,
        };

        if (food.id != null) {
          temp.push({
            id: item._id,
            foodId: food.id,
            sellerId: item.sellerId,
            status: item.status,
            orderId: item.orderId,
            otp: item.otp,
            favoriteType: tempfavorite.type,
            favoriteStatus: tempfavorite.status,
            name: food.name,
            youWant: food.youWant,
            type: food.type,
            homeDelivery: food.homeDelivery,
            homeDeliveryPrice: food.homeDeliveryPrice,
            quantity: food.quantity,
            description: food.description,
            availabilityTime: food.availabilityTime,
            foodStatus: food.status,
            pickupTime: food.pickupTime,
            price: food.price,
            userId: food.userId,
            images: food.images,
            foodCooked: food.foodCooked,
            cuisine: food.cuisine,
            address: food.address,
            timeStamp: item.timeStamp,
          });
        }
      }
    }
    const temporderResponseObj = await createTemporderObj(temp);
    order = temporderResponseObj;
    log.end();
    return order;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};
exports.get = get;
exports.getById = getById;
exports.create = create;
exports.update = update;
