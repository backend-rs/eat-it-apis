"use strict";
const FCM = require("fcm-node");
const { response } = require("express");
const serverKey =
  "AAAA-9IwZpo:APA91bGAb6KBxB2dEs7y_6Cicup1EFcpDCxEtEDI_sAtmBR4fsow9jYsBI7x90jABrjph6Q1mw_zi9ukmMPjE_nMs_jBzCT7rGr6a7AXQ8dNVw-sYDZWEGoaBHGnUIRX40v0DG494n4J";
// "AAAAnWNtWPc:APA91bGAdIHx-92dQsyhwUBeZhPhWo8JTAYie0I0DlbEygzqbWI0_we5xFyTVI-_t0nQTkmYU6fTruio2D80HqD0PC8kB5-kgiPCnBVUm5joxCZs9NmnVyW1x7J1W-IlRitHNVjJim05";
const fcm = new FCM(serverKey);

const sendNotification = async (model) => {
  try {
    let sendNotifications = await new db.pushNotification(model).save();
    return sendNotifications;
  } catch (err) {
    // log.end();
    throw new Error(err);
  }
};
const create = async (req, model, context, res) => {
  // const log = context.logger.start("services/pushNotifications");
  try {
    let results = [];
    let sendNotifications;
    let success = 0;
    let user = await db.user.findById(model.to);
    let datetemp = new Date();
    var message = {};
    if (model.from == "chatData") {
      message = {
        to: user.firebaseToken,
        collapse_key: "green",
        notification: {
          title: model.notification.title,
          body: model.notification.body,
        },
        data: {
          notificationType: model.data.notificationType,
          title: model.notification.title,
          body: model.notification.body,
          id: model.data.id,
          temp: datetemp,
          chatData: {
            orderId: model.chatData.orderId,
            username: model.chatData.userName,
            userId: model.chatData.userId,
            isExpired: model.chatData.isExpired,
          },
        },
      };
    } else {
      message = {
        to: user.firebaseToken,
        collapse_key: "green",
        notification: {
          title: model.notification.title,
          body: model.notification.body,
        },
        data: {
          notificationType: model.data.notificationType,
          title: model.notification.title,
          body: model.notification.body,
          id: model.data.id,
          temp: datetemp,
        },
      };
    }

    await fcm.send(message, function (err, response) {
      if (err) {
        console.log("Something has gone wrong!");
      } else {
        let temp = [];
        console.log("Successfully sent with response: ", JSON.parse(response));
        temp = JSON.parse(response);
        success = temp.success;
        results = temp.results;
        model.messageId = results[0].message_id;
        if (success == 1) {
          sendNotifications = sendNotification(model);
        }
      }
      // log.end();
      return sendNotifications;
    });
  } catch (err) {
    // log.end();
    throw new Error(err);
  }
};

const get = async (req, page, context) => {
  const log = context.logger.start("services/pushNotifications");
  try {
    let params = req.query;
    let query = {};
    let pushNotification;

    if (
      params.userId &&
      params.userId != null &&
      params.userId != undefined &&
      params.userId != ""
    ) {
      query = {
        to: {
          $eq: params.userId,
        },
      };
    }
    if (page != null && page != undefined && page != "") {
      pushNotification = await db.pushNotification
        .find(query)
        .skip(page.skipCount)
        .limit(page.items)
        .sort({
          timeStamp: -1,
        });
    } else {
      pushNotification = await db.pushNotification.find(query);
    }

    log.end();
    return pushNotification;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};
exports.create = create;

exports.get = get;

exports.create = create;
