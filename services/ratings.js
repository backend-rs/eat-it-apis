'use strict'
const response = require('../exchange/response');
const message = require('../helpers/message');
const create = async (req, model, context, res) => {
    const log = context.logger.start('services/ratings')
    let rating
    try {
        rating = await db.rating.findOne({
            'buyerId': {
                $eq: context.user._id
            },
            'sellerId': {
                $eq: model.sellerId
            }
        })
        if(rating){
            rating.ratingNo=model.ratingNo
            rating.sellerId=model.sellerId
            
        }else{

        rating = await new db.rating(model).save();
        rating.buyerId = context.user._id
        }
        rating.save()
        log.end();
        return rating
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, page, context) => {
    const log = context.logger.start(`services/ratings/get`)
    try {
        let params = req.query
        let query = {}
        let rating

        if (params.buyerId && (params.buyerId != null && params.buyerId != undefined && params.buyerId != '')) {
            query.buyerId = params.buyerId
        }
        if (page != null && page != undefined && page != "") {
            rating = await db.rating.find(query).skip(page.skipCount).limit(page.items).sort({
                timeStamp: -1
            })
        } else {
            rating = await db.rating.find(query)
        }

        log.end();
        return rating
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
exports.create = create

exports.get = get
