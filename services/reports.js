'use strict'
const response = require('../exchange/response');
const message = require('../helpers/message');
const report = require('../models/report');


const create = async (req, model, context, res) => {
    const log = context.logger.start('services/reports')
    let report
    try {



        report = await new db.report(model).save();
        let reason=await db.reason.findById(model.reasonId)
        if(reason){
            report.reasonName=reason.reason
        }

       
        
        report.save()
        log.end();
        return report
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, page, context) => {
    const log = context.logger.start(`services/reports/get`)
    try {
        let params = req.query
        let query = {}
        let report

                
        if (page != null && page != undefined && page != "") {
            report = await db.report.find().skip(page.skipCount).limit(page.items).sort({
                timeStamp: -1
            })
        }

        log.end();
        return report
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
exports.create = create

exports.get = get
