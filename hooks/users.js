'use strict'

const configure = (user) => {
    user.pre('save', function (next) {
        this.updatedAt = Date.now()
        next()
    })
}

exports.configure = configure
