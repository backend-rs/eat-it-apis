module.exports = [{
    url: '/',
    post: {
        summary: 'Create',
        description: 'Create favorite',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of favorite creation',
            required: true,
            schema: {
                $ref: '#/definitions/favoriteCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    get: {
        summary: 'Get',
        description: 'get all favorite',
        parameters: [
        //     {
        //     in: 'header',
        //     name: 'x-access-token',
        //     description: 'token to access api',
        //     required: true,
        //     type: 'string'
        // },
        {
            in: 'query',
            name: 'buyerId',
            description: '',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'status',
            description: 'like/dislike',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'pageNo',
            description: 'pageNo',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'items',
            description: 'items',
            required: false,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    }
},
{
    url: '/{id}',
   put: {
        summary: 'Update favorite',
        description: 'update favorite details',
        parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'path',
                name: 'id',
                description: 'favoriteId',
                required: true,
                type: 'string'
            },
            {
                in: 'body',
                name: 'body',
                description: 'Model of favorite update',
                required: true,
                schema: {
                    $ref: '#/definitions/favoriteUpdateReq'
                }
            }
        ],
        responses: {
            default: {
                description: {
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }

},
]