module.exports = [{
    url: '/',
    post: {
        summary: 'Create',
        description: 'Create payment',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of payment creation',
            required: true,
            schema: {
                $ref: '#/definitions/paymentCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    get: {
        summary: 'Get',
        description: 'get all payment',
        parameters: [
            {
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in: 'query',
            name: 'userId',
            description: '',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'pageNo',
            description: 'pageNo',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'items',
            description: 'items',
            required: false,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    }
},
{
    url: '/{id}',
    get: {
        summary: 'Get',
        description: 'get by Id',
        parameters: [
         {
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, 
         {
            in: 'path',
            name: 'id',
            description: 'paymentId',
            required: true,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
},
{
    url: '/createPayment',
    post: {
        summary: 'Create',
        description: 'Create payment',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of payment creation',
            required: true,
            schema: {
                $ref: '#/definitions/paymentCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
},
{
    url: '/paytmInitiateTokenOrStatus',
    post: {
        summary: 'Create',
        description: 'Create payment',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of payment creation',
            required: true,
            schema: {
                $ref: '#/definitions/paytmInitiateTokenOrStatusCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
},
]