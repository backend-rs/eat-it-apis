module.exports = [{
    url: '/',
    post: {
        summary: 'Create',
        description: 'Create cuisine',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of cuisine creation',
            required: true,
            schema: {
                $ref: '#/definitions/cuisineCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    get: {
        summary: 'Get',
        description: 'get all cuisine',
        parameters: [
        //     {
        //     in: 'header',
        //     name: 'x-access-token',
        //     description: 'token to access api',
        //     required: true,
        //     type: 'string'
        // },
        {
            in: 'query',
            name: 'status',
            description: 'active/inactive',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'pageNo',
            description: 'pageNo',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'items',
            description: 'items',
            required: false,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    }
},
{
    url: '/{id}',
   put: {
        summary: 'Update cuisine',
        description: 'update cuisine details',
        parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in: 'path',
                name: 'id',
                description: 'cuisineId',
                required: true,
                type: 'string'
            },
            {
                in: 'body',
                name: 'body',
                description: 'Model of cuisine update',
                required: true,
                schema: {
                    $ref: '#/definitions/cuisineUpdateReq'
                }
            }
        ],
        responses: {
            default: {
                description: {
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }

},
]