module.exports = [{
    url: '/',
    post: {
        summary: 'Create',
        description: 'Create rating',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of rating creation',
            required: true,
            schema: {
                $ref: '#/definitions/ratingCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    get: {
        summary: 'Get',
        description: 'get all rating',
        parameters: [
        //     {
        //     in: 'header',
        //     name: 'x-access-token',
        //     description: 'token to access api',
        //     required: true,
        //     type: 'string'
        // },
        {
            in: 'query',
            name: 'buyerId',
            description: '',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'pageNo',
            description: 'pageNo',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'items',
            description: 'items',
            required: false,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    }
}
]