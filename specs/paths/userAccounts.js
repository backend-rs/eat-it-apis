module.exports = [{
    url: '/',
    post: {
        summary: 'Create',
        description: 'Create userAccount',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of userAccount creation',
            required: true,
            schema: {
                $ref: '#/definitions/userAccountCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    get: {
        summary: 'Get',
        description: 'get all userAccount',
        parameters: [
            {
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
       {
            in: 'query',
            name: 'userId',
            description: 'userId',
            required: false,
            type: 'string'
        }
       ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    }
}
]