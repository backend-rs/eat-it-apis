module.exports = [{
    url: '/',
    post: {
        summary: 'Create',
        description: 'Create food',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of food creation',
            required: true,
            schema: {
                $ref: '#/definitions/foodCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    get: {
        summary: 'Get',
        description: 'get all food',
        parameters: [
      
        {
            in: 'query',
            name: 'userId',
            description: '',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'pageNo',
            description: 'pageNo',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'items',
            description: 'items',
            required: false,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    }
},
{
    url: '/{id}',
    get: {
        summary: 'Get',
        description: 'get food by Id',
        parameters: [
           {
            in: 'path',
            name: 'id',
            description: 'foodId',
            required: true,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
},
{
    url: '/search',
    get: {
        summary: 'search',
        description: 'get food',
        parameters: [
           {
            in: 'query',
            name: 'name',
            description: 'foods name',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'searchType',
            description: 'food/cuisine',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'cuisineId',
            description: '',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'cuisineName',
            description: '',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'cuisineName',
            description: '',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'userId',
            description: '',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'isfollow',
            description: 'true',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'forYou',
            description: 'true',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'nearByYou',
            description: 'true',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'latitude',
            description: 'true',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'longitude',
            description: 'true',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'foodCooked',
            description: 'restaurant/homemade',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'type',
            description: 'veg/nonVeg/langer',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'minPrice',
            description: '',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'isFoodFree',
            description: 'true',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'maxPrice',
            description: '',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'cost',
            description: 'paid/free',
            required: false,
            type: 'string'
        },
        {
            in: 'query',
            name: 'pageNo',
            description: 'pageNo',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'items',
            description: 'items',
            required: false,
            type: 'string'
        }
    ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
},
]