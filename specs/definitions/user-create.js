module.exports = {
    loginType: 'string',
    uId: 'string',
    tempToken: 'string',
    firstName: 'string',
    lastName: 'string',
    email: 'string',
    type: 'string',
    phone: 'string',
    password: 'string',
    // accountDetail: {
    //     accountNo: 'string',
    //     bankName: 'string',
    //     ifscCode: 'string'
    // },
    image: {
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string'
    },
    address: {
        line: 'string',
        pincode:'string',
        city: 'string',
        landmark: 'string',
        location : {
            type: 'string',
            coordinates: {
                type: [], // [<longitude>, <latitude>]
                index: "2dsphere", // create the geospatial index
              },
        }
    }
}


















// ,
// tempToken: {
//     type: 'string'
// },
// regToken: {
//     type: 'string'
// },

// expiryTime:{
//     type: 'string'
// },
// otp: {
//     type: 'string'
// },
// isVerified: {
//     type:Boolean,
//     default:false
// },
// token: {
//     type: 'string'
// },
// address: {
//     line1: 'string',
//     line2: 'string',
//     city: 'string',
//     district: 'string',
//     state: 'string',
//     pinCode: 'string',
//     country: 'string',
//     location: {
//         longitude: 'string',
//         latitude: 'string'
//     }
// }