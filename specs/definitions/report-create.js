'use strict'

// User Module
module.exports = {
   
   reasonId:'string',
  
   reportedTo:'string',
   reportedBy:'string'
}