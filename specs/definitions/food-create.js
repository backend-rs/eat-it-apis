module.exports = {
    isAdvanceFoodPost: 'boolean',
    advanceFoodDate: 'string',
    name:'string',
    youWant:'string',
    type:'string',
    homeDelivery:'string',
    homeDeliveryPrice:'number',
    quantity:'number',
    description:'string',
    availabilityTime:'number',
    pickupTime:{
        from:'date',
        to:'date'
    },
    foodCooked:'string',
    price:'number',
    address: {
        line: 'string',
        pincode:'string',
        city:'string',
        landmark:'string',
        location : {
            type: 'string',
            coordinates: {
                type: [], // [<longitude>, <latitude>]
                index: "2dsphere", // create the geospatial index
              },
        }
    },
    cuisine:[
        {
            id:'string'
        }
    ],
    images:[
        {
            url: 'string',
            thumbnail: 'string',
            resize_url:'string',
            resize_thumbnail: 'string'
        }
    ]
   
}


















// ,
// tempToken: {
//     type: 'string'
// },
// regToken: {
//     type: 'string'
// },

// expiryTime:{
//     type: 'string'
// },
// otp: {
//     type: 'string'
// },
// isVerified: {
//     type:Boolean,
//     default:false
// },
// token: {
//     type: 'string'
// },
// address: {
//     line1: 'string',
//     line2: 'string',
//     city: 'string',
//     district: 'string',
//     state: 'string',
//     pinCode: 'string',
//     country: 'string',
//     location: {
//         longitude: 'string',
//         latitude: 'string'
//     }
// }