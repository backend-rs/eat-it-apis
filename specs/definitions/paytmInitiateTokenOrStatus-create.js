
module.exports = {

    for:"string",
    paytmParams:{
        requestType: "string",
        mid: "string",
        websiteName: "string",
        orderId: "string",
        callbackUrl: "string",
        txnAmount: "string",
        userInfo: "string"
      }
   
}